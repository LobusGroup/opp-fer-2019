/*
This file includes all firebase cloud functions for the EkoReport application.

Currently active functions: 
 - onReportCreate: increment Users.totalReports 
 - onReportUpdate: updates Users.completedReports, Users.rejectedReports, Workers.totalCompletedReports, Worker.liveAssigned
 - onReportDelete: decrement Users.totalReports
 
 Parts of the code that are commented out are functional but not available in the free plan of Firebase Services therefore they are currently disabled.
*/


const functions = require('firebase-functions');
const admin = require('firebase-admin');
//const mapquest = require('mapquest');
//const util = require('util');
admin.initializeApp();
const db = admin.firestore();

//Report status constants
const STATUS_PENDING = 0;
const STATUS_REJECTED = 1;
const STATUS_ASSIGNED = 2;
const STATUS_COMPLETED = 3;



/*
//This function calculates the address using supplied coordinates
exports.getAddress = functions.https.onRequest((request, response) => {

	const promisedCoords = util.promisify(mapquest.reverse);
	
	promisedCoords({key: "4YDGxsexIMI9WVcIEQ4Cy3TocnrlNZYX", coordinates: { latitude: request.query.lat, longitude: request.query.lon } }).then((location)=>{

		console.log(location); 
			
		var adresa_str = "";
		if(location.street.length>0){
			adresa_str += location.street	
			if(location.geocodeQuality=="STREET")
				adresa_str += " Ulica, ";
			else
				adresa_str += ", "
		}
		adresa_str+=location.adminArea5;;
		response.send("Location is: "+adresa_str);
	}).catch(err =>{

		response.send("Location error: "+err);
		console.log('error: ', err)}
	
	);
	
	
	
});
*/

/*
//This function calculates the address automatically from reports.location GeoPoint
exports.reportAddressCalculator = functions.firestore.document('reports/{reportId}').onCreate((snap, context) =>{
	console.log("DEBUG-3");


	
	console.log("DEBUG1: "+snap.data().location.toString())
	var data = snap.data();
	var geoPoint = data.location;
	var lat = parseFloat(geoPoint._latitude);
	var lon = parseFloat(geoPoint._longitude);
	console.log("COORDINATES: "+lat+", "+lon);
	
	const promisedCoords = util.promisify(mapquest.reverse);
	
	promisedCoords({key: "4YDGxsexIMI9WVcIEQ4Cy3TocnrlNZYX", coordinates: { latitude: lat, longitude: lon } }).then((location)=>{
	if(location==null){
		console.log("ERROR WITH LOCATION, RETURNING NULL");
		return null;
	}
	
	var adresa_str = "";
	if(location.street.length>0){
		adresa_str += location.street	
		if(location.geocodeQuality=="STREET")
			adresa_str += " Ulica, ";
		else
			adresa_str += ", "
	}
	adresa_str+=location.adminArea5;;
	console.log(adresa_str);
	return snap.ref.set({
        address: adresa_str
      }, {merge: true});
	
	}).catch(err => console.log('error: ', err));
	
	
});

*/


exports.onReportCreate = functions.firestore.document('reports/{reportId}').onCreate((snap, context) =>{
	
	//Convert uid to working string...
	var uid_user = snap.data().uidUser.toString().replace(/\s/g, '');
	
	//Use this to increment counters
	const incrementC = admin.firestore.FieldValue.increment(1);
		
	//Increment users total reports		
	const userRefI = db.collection('users').doc(uid_user);
	userRefI.update({totalReports: incrementC});
	return null;
	
});

exports.onReportDelete = functions.firestore.document('reports/{reportId}').onDelete((snap, context) =>{
	
	//Convert uid to working string...
	var uid_user = snap.data().uidUser.toString().replace(/\s/g, '');
	
	//Use this to increment counters
	const decrementC = admin.firestore.FieldValue.increment(-1);
		
	//Increment users total reports		
	const userRefI = db.collection('users').doc(uid_user);
	userRefI.update({totalReports: decrementC});
	return null;
	
});

exports.onReportUpdate = functions.firestore.document('reports/{reportId}').onUpdate((change, context) =>{
	
	var getDoc = null;
	
	var new_status = change.after.data().statusCode;
	var old_status = change.before.data().statusCode;
	var report_id = context.params.reportId;
	var uid_user = change.after.data().uidUser.toString().replace(/\s/g, '');
	var fcm_token = "Unknown";
	var uid_worker_old = null;
	var uid_worker_new = null;
	
	//If we expect we might need the uidWorker, fetch it
	if((new_status === STATUS_COMPLETED||new_status === STATUS_ASSIGNED)&&change.after.data().uidWorker != null) 
		uid_worker_new = change.after.data().uidWorker.toString().replace(/\s/g, '');
	if(old_status === STATUS_ASSIGNED&&change.before.data().uidWorker != null)
		uid_worker_old = change.before.data().uidWorker.toString().replace(/\s/g, '');
	
	//On report completed, notify reporter and update reporter/worker ratings
	if(new_status === STATUS_COMPLETED && old_status != STATUS_COMPLETED){

		//Use this to increment counters
		const incrementC = admin.firestore.FieldValue.increment(1);

		//Increment users completed reports		
		const userRefI = db.collection('users').doc(uid_user);
		userRefI.update({completedReports: incrementC});

		//Increment workers completed reports	
		const workerRefI = db.collection('workers').doc(uid_worker_new);
		workerRefI.update({totalCompletedReports: incrementC});

		//Send notification to original reporter
		let userRef = db.collection('users').doc(uid_user);
		getDoc = userRef.get()
		.then(doc => {
			fcm_token = doc.data().FCMtoken;
			
			//If notification can be sent...
			if(fcm_token != null){
				//Form the FCM message here:
				//TODO: This should be updated once the android app is finished and notification format is decided!
				const payload = {
				notification: {
					title: 'Your report was resolved!',
					body: 'Your report '+change.after.data().title+' was accepted and problem was fixed. Thank you for using EkoReport! :)'
					}
				};
				admin.messaging().sendToDevice(fcm_token, payload);  	
			}			
		})
		.catch(err => {
			//This shouldn't happen
			console.log('Error getting document', err);
		});		
	}
	
	//If report has been rejected, update user rating and send notification
	if(new_status === STATUS_REJECTED && old_status != STATUS_REJECTED){
		
		//Use this to increment counters
		const incrementR = admin.firestore.FieldValue.increment(1);
		
		//Increment users rejected reports		
		const userRefI = db.collection('users').doc(uid_user);
		userRefI.update({rejectedReports: incrementR});
		
		//Send notification to original reporter	
		let userRef = db.collection('users').doc(uid_user);
		getDoc = userRef.get()
		.then(doc => {
			fcm_token = doc.data().FCMtoken;
			
			if(fcm_token != null){
				//Form the message here:
				//TODO: This should be updated once the android app is finished and notification format is decided!
				const payload = {
				notification: {
					title: 'Your report was rejected :(',
					body: 'Moderators rejected your report: '+change.after.data().title
					}
				};
				admin.messaging().sendToDevice(fcm_token, payload);	
			}			
		})
		.catch(err => {
			//This shouldn't happen
			console.log('Error getting document', err);
		});		
	}
	
	
	//If report has just been assigned increment liveAssigned
	if(new_status === STATUS_ASSIGNED && old_status != STATUS_ASSIGNED){		
		//Increment workers currently assigned
		const incrementW = admin.firestore.FieldValue.increment(1);
		const workerRefI = db.collection('workers').doc(uid_worker_new);
		workerRefI.update({liveAssigned: incrementW});		
	}
	
	//If report was for any reason removed from worker, decrement liveAssigned
	if(new_status != STATUS_ASSIGNED && old_status === STATUS_ASSIGNED){		
		//Decrement workers currently assigned
		const decrement = admin.firestore.FieldValue.increment(-1);
		const workerRefI = db.collection('workers').doc(uid_worker_old);
		workerRefI.update({liveAssigned: decrement});		
	}	
	
	//If report was reassigned transfer liveAssigned to new worker
	if(new_status === STATUS_ASSIGNED && old_status === STATUS_ASSIGNED){		
		//Decrement workers currently assigned
		const decrementT = admin.firestore.FieldValue.increment(-1);
		const incrementT = admin.firestore.FieldValue.increment(1);
		
		const workerRefT = db.collection('workers').doc(uid_worker_old);
		workerRefT.update({liveAssigned: decrementT});	

		const workerRefT2 = db.collection('workers').doc(uid_worker_new);
		workerRefT2.update({liveAssigned: incrementT});			
	}	
	
	return getDoc;

});