/*
This script is used to show all available users. It is a part of EkoReport Admin Console.
It's loaded from index.html. 
Lists all users with colors corresponding to their current roles and lets you open the editor for each user.
*/

var popup;
var userstated = false;

//This is executed once firebase functionality is loaded. Used to initialize the API.
document.addEventListener('DOMContentLoaded', function() {
	
	//Used to verify wether user has logged in and has admin privileges or not.
	firebase.auth().onAuthStateChanged(function(user) {
		
		//console.log(user);
		if(!userstated){
			userstated = true;
			if (user != null) {
				var user = firebase.auth().currentUser;
				if(user != null){
					//First check if logged-in user is admin
					var db = firebase.firestore();
					var docRef = db.collection("admins").doc(user.uid);
					docRef.get().then(function(doc) {
						
						//If admin is logged in, display all the data
						if (doc.exists) {
							//Enable the main UI
							document.getElementById("main-ui").style.display = "block";							
							//Add users to list
							fillUserList();
							
						} else {
							//If user is not an amin and somehow managed to log in, kick them out and redirect to login page.
							logout();
							window.location.replace("login.html");
						}
					}).catch(function(error) {
						//This should never happen! Yet it does for literally everyone who is not an admin, therefore, this calls for a logout....
						console.error(error);
						logout();
						window.location.replace("login.html");
					});
				}
			} else {
				//If administrator is not logged in redirect to login page
				//console.log(firebase.auth().currentUser);
				window.location.replace("login.html");
			}
		}
	});

	//Load firebase app here
	try {
	  let app = firebase.app();
	  let features = ['auth', 'database', 'messaging', 'storage'].filter(feature => typeof app[feature] === 'function');
	} catch (e) {
	  console.error(e);
	}
});

//Log out current user
function logout(){
	//console.log("PERFORMING LOGOUT");
	firebase.auth().signOut();
	window.location.replace("login.html");
}

//This function adds users from db to the HTML
function fillUserList(){
	
	//Every other user has a slightly darker color
	var second = false;
	var db = firebase.firestore();
	
	//Sort users by lastName and add them to list
	db.collection("users").orderBy("lastName")
    .get()
    .then(function(querySnapshot) {
        querySnapshot.forEach(function(doc) {
			
			//Create display item for the user
			var node = document.createElement("A"); 
			node.setAttribute("id",doc.id);
			
			//Every other user should have a different color
			if(second){
				node.setAttribute("style","background-color: var(--forest-green-light)");
			}
			var textnode = document.createTextNode(doc.data().lastName + " "+ doc.data().firstName + " (" + doc.data().email + ")"); 
			node.appendChild(textnode);    
			node.setAttribute("href","javascript:editUser(\""+doc.id+"\")")
			document.getElementById("user-list-nav").appendChild(node);
			second = !second;
        });

    }).catch(function(error) {
        //This should never happen! Maybe add some redundant code here at some point just in case.
		console.error(error);
    });	
	
	//Change colors of specific roles	
	
	//Iterate moderators
	db.collection("moderators")
    .get()
    .then(function(querySnapshot) {
        querySnapshot.forEach(function(doc) {
			var node = document.getElementById(doc.id); 
			node.setAttribute("style","background-color: var(--moderator-color)");
        });
    }).catch(function(error) {
        //This should never happen! Maybe add some redundant code here at some point just in case.
		console.error(error);
    });

	//Iterate workers
	db.collection("workers")
    .get()
    .then(function(querySnapshot) {
        querySnapshot.forEach(function(doc) {
			var node = document.getElementById(doc.id); 
			node.setAttribute("style","background-color: var(--worker-color)");
        });
    }).catch(function(error) {
        //This should never happen! Maybe add some redundant code here at some point just in case.
		console.error(error);
    });
	
	//Iterate admins
	db.collection("admins")
    .get()
    .then(function(querySnapshot) {
        querySnapshot.forEach(function(doc) {
			var node = document.getElementById(doc.id); 
			node.setAttribute("style","background-color: var(--admin-color)");
        });
    }).catch(function(error) {
        //This should never happen! Maybe add some redundant code here at some point just in case.
		console.error(error);
    });
	
}

//This function is called from edituser.html and is used to close it reflect any changes made there on the main UI
function closePopup(changecolor){
	
	//Close the iframe "popup"
	document.getElementById("edit-iframe").style.display = "none";
	document.getElementById("hidden-overlay").style.display = "none";
	
	//If users role has changed, update it 
	if(changecolor){
		var chg_uid = localStorage.getItem("CHG_uid");
		var chg_type = localStorage.getItem("CHG_type");
		document.getElementById(chg_uid).setAttribute("style","background-color: var(--"+chg_type+"-color)");
	}
}

//Displays edituser.html in an iframe and sends the uid of the user we want to edit
function editUser(uid){
	localStorage.setItem("editUID",uid);
	localStorage.setItem("anychanges","false");
	//popup = window.open('edituser.html','Edit User','width=640,height=640,resizable=yes'); 
	document.getElementById("edit-iframe").contentDocument.location.reload(true);
	document.getElementById("edit-iframe").style.display = "block";
	document.getElementById("hidden-overlay").style.display = "block";
}


