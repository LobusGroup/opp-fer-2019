/*
This script is used to edit roles of a specific user. It is a part of EkoReport Admin Console.
It's loaded from edituser.html with the UID of a specific user. 
User's current data is loaded and displayed with available options for changing roles/categories.
*/

//Global defines
var edit_uid = "Unknown";
var db;
var all_cats = [];
var selected_cats = [];
var initial_cats = [];
var worker_tag = false;
var user_banned = false;
var userstated = false;
var firstName = "";
var lastName = "";

//This is executed once firebase functionality is loaded. Used to initialize the API.
document.addEventListener('DOMContentLoaded', function() {
	
	//Used to verify wether user has logged in and has admin privileges or not.
	firebase.auth().onAuthStateChanged(function(user) {
		if(!userstated){
			userstated = true;
			if (user) {
				//UID of a currently logged in user.
				var user = firebase.auth().currentUser;
				if(user != null){
					
					//Connect to cloud firestore
					db = firebase.firestore();
					
					//check if logged in user is admin
					var docRef = db.collection("admins").doc(user.uid);
					docRef.get().then(function(doc) {
						//If admin is logged in, display all the data
						if (doc.exists) {
							
							//Get the UID of the user we want to edit from localStorage
							edit_uid = localStorage.getItem("editUID");
							
							//Edit HTML display options
							document.getElementById("is_login").innerHTML = "User role editor";
							document.getElementById("main-ui").style.display = "block";		
							
							//Add user data here
							var docRef = db.collection("users").doc(edit_uid);
								docRef.get().then(function(doc) {
									if (doc.exists) {
										document.getElementById("user-details").innerHTML = doc.data().firstName + " "+ doc.data().lastName + " </br>" + doc.data().email + "";
										user_banned = doc.data().banned;
										firstName = doc.data().firstName;
										lastName = doc.data().lastName;
									} else {
										//This should never happen! Maybe add some redundant code here at some point just in case.
									}
								}).catch(function(error) {
									//This should never happen! Maybe add some redundant code here at some point just in case.
									console.error(error);
								});
							//Start recursive role checks here
							isModerator();
														
						} else {
							//If user is not an amin and somehow managed to log in, kick them out and redirect to login page.
							logout();
							window.location.replace("login.html");
						}
					}).catch(function(error) {
						//This should never happen! Yet it does for literally everyone who is not an admin, therefore, this calls for a logout....
						//console.error(error);
						logout();
						window.location.replace("login.html");
						
					});
				}
			} else {
				//If administrator is not logged in redirect to login page
				window.location.replace("login.html");
			}
		}
	});
	
	//Load firebase app here
	try {
	  let app = firebase.app();
	  let features = ['auth', 'database', 'messaging', 'storage'].filter(feature => typeof app[feature] === 'function');
	} catch (e) {
	  console.error(e);
	}
});



	

//This function performs some checks and then closes the edituser popup
function closePopup(){
	//If users role was changed this will alert the main UI to change that users color
	var anychanges = (localStorage.getItem("anychanges")==="true");	
	
	//Check for category changes and max assigned changes if the user is worker
	if(worker_tag==true){
		var maxAssigned = parseInt(document.getElementById("max-assigned-input").value);
		
		//If any number is entered int othe max assigned field
		var assignedchanged = (maxAssigned>0&&maxAssigned<100000);
		
		//Check for changes in selected categories
		selected_cats = [];
		all_cats.forEach(addSelectedCats);
		
		//If there are unsaved changes show the confirmation dialog and then close the popup window
		if(JSON.stringify(selected_cats)!=JSON.stringify(initial_cats)||assignedchanged){				
			if(confirm("Are you sure? Any unsaved changes will be lost!")){
					window.top.closePopup(anychanges);
			}
		}else{
			window.top.closePopup(anychanges);
		}		
	}else{
		window.top.closePopup(anychanges);
	}
}


//This function adds or removes user from moderators (adds if active==true, removes otherwise)
function setModerator(active){
	//Update the color in the main UI
	changeRoleColor(active?"moderator":"user");
	
	if(active){
		//If active == true add to moderators and remove from workers for promotion and just in case		
		db.collection("workers").doc(edit_uid).delete()
		.then(function() {
			db.collection("moderators").doc(edit_uid).set({})
			.then(function() {
				//Reload page with the new role
				location.reload();
			});
		});		
	}else{
		//If active == false remove from moderators
		db.collection("moderators").doc(edit_uid).delete()
		.then(function() {
			//Reload page with the new role
			location.reload();
		});
	}	
}

//This function adds user to workers
function setWorker(active){
	//Update the color in the main UI
	changeRoleColor(active?"worker":"user");
	
	if(active){
		//If active, add empty record into workers
		db.collection("workers").doc(edit_uid).set({
			categories: [],
			liveAssigned: 0,
			firstName: firstName,
			lastName: lastName,
			maxAssigned: 20,	//CHANGE DEFAULT "maxAssigned" HERE!
			totalCompletedReports: 0,			
		})
			.then(function() {
				//Reload page with the new role
				location.reload();
		});	
	}else{
		//If not active, remove record from workers
		db.collection("workers").doc(edit_uid).delete()
		.then(function() {
			//Reload page with the new role
			location.reload();
		});	
	}
}

//Check if category {value} is selected and add it to selected categories
function addSelectedCats(value){
	if(document.getElementById("checkbox-"+value).checked){
		selected_cats.push(parseInt(value));
	}
}

//This function bans/unbans a user
function setBanned(ban){
	db.collection("users").doc(edit_uid).update({
		"banned": ban	
	}).then(function() {
		//Reload page with the new state
		location.reload();
	});	
	
}

//Update selected options for worker
function updateCategories(){
	//update maxAssigned if there was a change
	var maxAssigned = parseInt(document.getElementById("max-assigned-input").value);
	if(maxAssigned>0&&maxAssigned<100000){
		db.collection("workers").doc(edit_uid).update({
			"maxAssigned": parseInt(maxAssigned)		
		}).then(function() {});	
	}
	
	var miscData = document.getElementById("misc-data-input").value
	db.collection("workers").doc(edit_uid).update({
			"miscData": miscData
		}).then(function() {});	
	
	//Update currently selected categories
	selected_cats = [];
	all_cats.forEach(addSelectedCats);
	
	//Write changes to db
	db.collection("workers").doc(edit_uid).update({
		"categories": selected_cats		
	})
		.then(function() {
			//Reload page with the new roles
			location.reload();
	});		
}


//This function checks if user is moderator and updates the HTML accordingly
function isModerator(){
	//Check if user is Moderator
	var docRef = db.collection("moderators").doc(edit_uid);
	docRef.get().then(function(doc) {
		if (doc.exists) {
			//Is Moderator
			document.getElementById("user-role").innerHTML = "Moderator";
			var demodBtn = document.createElement("button");
			demodBtn.setAttribute("class","role-button");
			demodBtn.setAttribute("onclick","setModerator(false)");
			demodBtn.innerHTML = "Remove from moderators";
			document.getElementById("categories-nav").appendChild(demodBtn);			
		} else {
			//Is not Moderator, worker maybe?			
			isWorker();
		}
	}).catch(function(error) {
		//This should never happen! Maybe add some redundant code here at some point just in case.
		console.error(error);
	});	
}

//This function checks if user is worker and updates the HTML accordingly
function isWorker(){
	var docRef = db.collection("workers").doc(edit_uid);
	docRef.get().then(function(doc) {
		if (doc.exists) {
			//Is Worker
			document.getElementById("user-role").innerHTML = "Worker";
			worker_tag = true;
			addCategories(doc.data().categories,doc.data().maxAssigned,(doc.data().liveAssigned!=0),doc.data().miscData)
		} else {
			//Is not Worker
			document.getElementById("user-role").innerHTML = "Regular user";
			
			//Add "Make moderator" button
			var modBtn = document.createElement("button");
			modBtn.setAttribute("onclick","setModerator(true)");
			modBtn.setAttribute("class","role-button");
			modBtn.innerHTML = "Make moderator";
			document.getElementById("categories-nav").appendChild(modBtn);
			
			//Add "Make worker" button
			var workerBtn = document.createElement("button");
			workerBtn.setAttribute("onclick","setWorker(true)");
			workerBtn.setAttribute("class","role-button");
			workerBtn.innerHTML = "Make worker";
			document.getElementById("categories-nav").appendChild(workerBtn);	
			
			//Add "ban/unban" buttons
			var banBtn = document.createElement("button");
			banBtn.setAttribute("class","ban-button");
			if(!user_banned){
				banBtn.setAttribute("onclick","setBanned(true)");
				banBtn.innerHTML = "Ban user";
			} else {
				banBtn.setAttribute("onclick","setBanned(false)");
				banBtn.innerHTML = "Un-ban user";
			}
			document.getElementById("categories-nav").appendChild(banBtn);			
		
		}
	}).catch(function(error) {
		//This should never happen! Maybe add some redundant code here at some point just in case.
		console.error(error);
	});	
	
}

//This function marks the user for color change on the main UI
function changeRoleColor(type){
	localStorage.setItem("anychanges","true");
	localStorage.setItem("CHG_uid",edit_uid);
	localStorage.setItem("CHG_type",type);	
}

function addCategories(activecats,maxAssigned,hasAssigned,miscData){
	db.collection("categories")
    .get()
    .then(function(querySnapshot) {
		//Add a checkbox for each category
        querySnapshot.forEach(function(doc) {		
			all_cats.push(doc.id);			
			
			//Use magic to create a working checkbox.
			var div = document.createElement("div");
			div.setAttribute("class","checkbox-div");
			var node = document.createElement("input"); 
			node.setAttribute("value",doc.data().titleEN); //Change to ".titleHR" to use croatian category names  
			node.setAttribute("name",doc.id);  
			node.setAttribute("type","checkbox");  
			node.setAttribute("id","checkbox-"+doc.id);
			//If current worker has this category, check the checkbox
			if(activecats.includes(parseInt(doc.id))){
				node.setAttribute("checked","true");
				initial_cats.push(parseInt(doc.id));
			}
			var label = document.createElement('label'); 
			label.htmlFor = "checkbox-"+doc.id;			
			label.appendChild(document.createTextNode(doc.data().titleEN)); 
			div.appendChild(label);
			div.appendChild(node);			
			document.getElementById("categories-nav").appendChild(div);
        });
		
		//Add maxAssigned input field
		var maDiv = document.createElement("div");
		maDiv.setAttribute("class","checkbox-div");
		var maInput = document.createElement("input");
		var maLabel = document.createElement('label'); 			
		maLabel.appendChild(document.createTextNode("Max assigned reports:")); 
		maInput.setAttribute("placeholder",maxAssigned);
		maInput.setAttribute("id","max-assigned-input");
		maDiv.appendChild(maLabel);
		maDiv.appendChild(maInput);
		document.getElementById("categories-nav").appendChild(maDiv);
		
		//Add Misc Data input field
		var mdDiv = document.createElement("div");
		mdDiv.setAttribute("class","checkbox-div");
		var mdInput = document.createElement("input");
		var mdLabel = document.createElement('label'); 			
		mdLabel.appendChild(document.createTextNode("Stationed at:")); 
		mdInput.setAttribute("placeholder",miscData);
		mdInput.setAttribute("id","misc-data-input");
		mdDiv.appendChild(mdLabel);
		mdDiv.appendChild(mdInput);
		document.getElementById("categories-nav").appendChild(mdDiv);
		
		//Add other worker option buttons
		var currBtn = document.createElement("button");
		currBtn.setAttribute("class","role-button");
		currBtn.setAttribute("onclick","updateCategories()");
		currBtn.innerHTML = "Update categories and max reports";
		document.getElementById("categories-nav").appendChild(currBtn);
		
		//If worker has assigned reports don't allow their removal!
		if(!hasAssigned){
			currBtn = document.createElement("button");
			currBtn.setAttribute("class","role-button");
			currBtn.setAttribute("onclick","setModerator(true)");
			currBtn.innerHTML = "Make moderator (remove from workers)";
			document.getElementById("categories-nav").appendChild(currBtn);
			
			currBtn = document.createElement("button");
			currBtn.setAttribute("class","role-button");
			currBtn.setAttribute("onclick","setWorker(false)");
			currBtn.innerHTML = "Make regular user (remove from workers)";
			document.getElementById("categories-nav").appendChild(currBtn);
		}else{
			var haLabel = document.createElement('label'); 			
			haLabel.appendChild(document.createTextNode("Warning: Worker has assigned reports! Changing roles is currently not possible!")); 
			document.getElementById("categories-nav").appendChild(haLabel);
		}

		
    })
    .catch(function(error) {
        //This should never happen! Maybe add some redundant code here at some point just in case.
		console.error(error);
    });	
	
}

//Log out current user
function logout(){
	firebase.auth().signOut();
	window.location.replace("login.html");
}

