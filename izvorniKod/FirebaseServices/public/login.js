/*
This script is used to allow administrator to log in. It is a part of EkoReport Admin Console. 
It allows users to enter their credentials and performs login and admin check and if all is ok allows admin to access the console.
*/
  
//This is executed once firebase functionality is loaded. Used to initialize the API.
document.addEventListener('DOMContentLoaded', function() {
	firebase.auth().onAuthStateChanged(function(user) {
		if (user) {
			//First check if logged-in user is admin
			var db = firebase.firestore();
			var docRef = db.collection("admins").doc(user.uid);
			docRef.get().then(function(doc) {
				
				//If admin is looged in redirect to index.html
				if (doc.exists) {
					window.location.replace("index.html");
				} else {
					//If user is not an amin and somehow managed to log in, kick them out and redirect to login page.
					logout();
					document.getElementById('error-text').innerHTML = "Error: you are not admin!";
					document.getElementById('error-text').setAttribute("style","color:red");
				}
			}).catch(function(error) {
				//This should never happen! Yet it does for literally everyone who is not an admin, therefore, this calls for a logout....
				//console.error(error);
				logout();
				document.getElementById('error-text').innerHTML = "Error: you are not admin!";		
				document.getElementById('error-text').setAttribute("style","color:red");				
			});
		}
	});
	
	//Load firebase app here
	try {
	  let app = firebase.app();
	  let features = ['auth', 'database', 'messaging', 'storage'].filter(feature => typeof app[feature] === 'function');
	  //document.getElementById('load').innerHTML = `Firebase SDK loaded with ${features.join(', ')}`;
	} catch (e) {
	  console.error(e);
	  //document.getElementById('load').innerHTML = 'Error loading the Firebase SDK, check the console.';
	}
});


//Listeners to allow enter pressing instead of button click
document.getElementById("email_field")
    .addEventListener("keyup", function(event) {
    event.preventDefault();
    if (event.keyCode === 13) {
        login();
    }
});
document.getElementById("password_field")
    .addEventListener("keyup", function(event) {
    event.preventDefault();
    if (event.keyCode === 13) {
        login();
    }
});

//This function performs all email/password checks and logs the user in
function login(){

  var userEmail = document.getElementById("email_field").value;
  var userPass = document.getElementById("password_field").value;

  firebase.auth().signInWithEmailAndPassword(userEmail, userPass).catch(function(error) {
	  
    // Handle Errors here.
    var errorCode = error.code;
    var errorMessage = error.message;
	if(errorCode === "auth/wrong-password"){
		document.getElementById('error-text').innerHTML = "Error: wrong login.";
		document.getElementById('error-text').setAttribute("style","color:red");
	}else if(errorCode === "auth/user-not-found"){
		document.getElementById('error-text').innerHTML = "Error: wrong login.";
		document.getElementById('error-text').setAttribute("style","color:red");
	}else if(errorCode === "auth/invalid-email"){
		document.getElementById('error-text').innerHTML = "Error: wrong login.";
		document.getElementById('error-text').setAttribute("style","color:red");
	}
  });
}

//This logs the user out
function logout(){
  firebase.auth().signOut();
}
