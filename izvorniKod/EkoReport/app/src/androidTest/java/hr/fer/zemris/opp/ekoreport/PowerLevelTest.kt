package hr.fer.zemris.opp.ekoreport

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import hr.fer.zemris.opp.ekoreport.TestUtil.childAtPosition
import hr.fer.zemris.opp.ekoreport.view.MainActivity
import org.hamcrest.Matchers.allOf
import org.hamcrest.Matchers.not
import org.junit.FixMethodOrder
import org.junit.Rule
import org.junit.Test
import org.junit.runners.MethodSorters

@LargeTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class PowerLevelTest {

    @Rule
    @JvmField
    var mActivityTestRule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun a_RegularPowerLevelTest() {
        Thread.sleep(3000)
        TestUtil.login("d10810589@urhen.com", "sifra1234")

        Thread.sleep(3000)

        val viewGroup = onView(
            allOf(withId(R.id.toolbar), isDisplayed())
        )
        viewGroup.check(matches(isDisplayed()))

        val appCompatImageButton = onView(
            allOf(
                withContentDescription("Open navigation drawer"), childAtPosition(
                    allOf(
                        withId(R.id.toolbar)
                    ), 1
                ), isDisplayed()
            )
        )
        appCompatImageButton.perform(click())

        Thread.sleep(1000)

        val button = onView(
            allOf(withId(R.id.btnRoleWorker))
        )
        button.check(matches(not(isDisplayed())))

        val button2 = onView(
            allOf(withId(R.id.btnRoleModerator))
        )
        button2.check(matches(not(isDisplayed())))

        TestUtil.signOut(R.id.btnSignOutDrawer)
    }

    @Test
    fun b_WorkerPowerLevelTest() {
        Thread.sleep(3000)

        TestUtil.login("d10811344@urhen.com", "sifra1234")

        Thread.sleep(3000)

        val appCompatImageButton = onView(
            allOf(
                withContentDescription("Open navigation drawer"), childAtPosition(
                    allOf(
                        withId(R.id.toolbar)
                    ), 1
                ), isDisplayed()
            )
        )
        appCompatImageButton.perform(click())

        Thread.sleep(1000)

        val button = onView(
            allOf(withId(R.id.btnRoleWorker), isDisplayed())
        )
        button.check(matches(isDisplayed()))

        TestUtil.signOut(R.id.btnSignOutDrawer)
    }

    @Test
    fun c_ModeratorPowerLevelTest() {
        Thread.sleep(3000)

        TestUtil.login("d10811509@urhen.com", "sifra1234")

        Thread.sleep(3000)

        val appCompatImageButton = onView(
            allOf(
                withContentDescription("Open navigation drawer"), childAtPosition(
                    allOf(
                        withId(R.id.toolbar)
                    ), 1
                ), isDisplayed()
            )
        )
        appCompatImageButton.perform(click())

        Thread.sleep(1000)

        val button2 = onView(
            allOf(
                withId(R.id.btnRoleModerator), isDisplayed()
            )
        )
        button2.check(matches(isDisplayed()))

        TestUtil.signOut(R.id.btnSignOutDrawer)
    }

}
