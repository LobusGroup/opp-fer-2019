package hr.fer.zemris.opp.ekoreport

import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.scrollTo
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import hr.fer.zemris.opp.ekoreport.TestUtil.childAtPosition
import hr.fer.zemris.opp.ekoreport.view.MainActivity
import org.hamcrest.Matchers.`is`
import org.hamcrest.Matchers.allOf
import org.junit.Rule
import org.junit.Test

@LargeTest
class AssignReportTest {

    @Rule
    @JvmField
    var mActivityTestRule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun assignReportTest() {

        Thread.sleep(2000)
        TestUtil.login("d10811509@urhen.com", "sifra1234")
        Thread.sleep(4000)

        onView(
            allOf(
                withContentDescription("Open navigation drawer"), isDisplayed()
            )
        ).perform(click())

        try {
            onView(withText("Switch to Moderator")).perform(click())
            Thread.sleep(1000)
        } catch (_: Exception) {
            // View is not in hierarchy -> Moderator is already active
            Espresso.pressBack()
            Thread.sleep(1000)
        }

        val materialCardView = onView(
            allOf(
                withId(R.id.singleReportRoot),
                childAtPosition(
                    allOf(
                        withId(R.id.rvAllReports),
                        childAtPosition(
                            withClassName(`is`("androidx.constraintlayout.widget.ConstraintLayout")),
                            1
                        )
                    ),
                    0
                ),
                isDisplayed()
            )
        )
        materialCardView.perform(click())
        Thread.sleep(2000)

        val textView = onView(
            allOf(
                withId(R.id.tvStatus), withText("Pending")
            )
        )
        textView.check(matches(isDisplayed()))
        Thread.sleep(1000)

        val materialButton2 = onView(
            allOf(
                withId(R.id.btnAssign), withText("Assign to Worker")
            )
        )
        materialButton2.perform(scrollTo(), click())
        Thread.sleep(2000)

        val linearLayout = onView(
            allOf(
                withId(R.id.singleWorkerRoot),
                childAtPosition(
                    allOf(
                        withId(R.id.rvAvailableWorkers),
                        childAtPosition(
                            withClassName(`is`("android.widget.LinearLayout")),
                            0
                        )
                    ),
                    0
                ),
                isDisplayed()
            )
        )
        linearLayout.perform(click())
        Thread.sleep(2000)

        val textView2 = onView(
            allOf(
                withId(R.id.tvStatus), withText("Assigned")
            )
        )
        textView2.check(matches(isDisplayed()))
        Thread.sleep(1000)

        val materialButton4 = onView(
            allOf(
                withId(R.id.btnUnassign), withText("Unassign")
            )
        )
        materialButton4.perform(scrollTo(), click())
        Thread.sleep(2000)

        val textView3 = onView(
            allOf(
                withId(R.id.tvStatus), withText("Pending")
            )
        )
        textView3.check(matches(isDisplayed()))
        Thread.sleep(1000)

        val appCompatImageButton = onView(
            allOf(
                withContentDescription("Navigate up"), isDisplayed()
            )
        )
        appCompatImageButton.perform(click())
        Thread.sleep(2000)

        val appCompatImageButton2 = onView(
            allOf(
                withContentDescription("Open navigation drawer"), isDisplayed()
            )
        )
        appCompatImageButton2.perform(click())
        Thread.sleep(2000)

        TestUtil.signOut(R.id.btnSignOutDrawer)

    }

}
