package hr.fer.zemris.opp.ekoreport

import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import androidx.test.rule.GrantPermissionRule
import hr.fer.zemris.opp.ekoreport.TestUtil.childAtPosition
import hr.fer.zemris.opp.ekoreport.view.MainActivity
import org.hamcrest.Matchers
import org.hamcrest.Matchers.allOf
import org.junit.Rule
import org.junit.Test

@LargeTest
class AddNewReportTest {

    @Rule
    @JvmField
    var mActivityTestRule = ActivityTestRule(MainActivity::class.java)

    @Rule
    @JvmField
    var mGrantPermissionRule: GrantPermissionRule =
        GrantPermissionRule.grant(
            "android.permission.ACCESS_FINE_LOCATION",
            "android.permission.ACCESS_COARSE_LOCATION"
        )

    @Test
    fun addNewReportTest() {

        Thread.sleep(2000)
        TestUtil.login("d10810589@urhen.com", "sifra1234")
        Thread.sleep(4000)

        val appCompatImageButton4 = onView(
            allOf(
                withContentDescription("Open navigation drawer"), isDisplayed()
            )
        )
        appCompatImageButton4.perform(click())
        Thread.sleep(2000)

        val numberOfReports3 = onView(
            allOf(
                withId(R.id.headerTotalReports), isDisplayed()
            )
        )
        numberOfReports3.check(matches(withText("1")))
        Thread.sleep(1000)
        Espresso.pressBack()
        Thread.sleep(2000)

        val floatingActionButton = onView(
            allOf(
                withId(R.id.fabNewReport), isDisplayed()
            )
        )
        floatingActionButton.perform(click())
        Thread.sleep(3000)

        val textInputEditText = onView(
            allOf(
                withId(R.id.etEditTitle)
            )
        )
        textInputEditText.perform(scrollTo(), replaceText("Mrtva životinja"), closeSoftKeyboard())
        Thread.sleep(2000)

        val textInputEditText2 = onView(
            allOf(
                withId(R.id.etEditDescription)

            )
        )
        val materialButton5 = onView(
            allOf(
                withId(R.id.btnCategorySelect), withText("Category")
            )
        )
        materialButton5.perform(scrollTo(), click())
        Thread.sleep(1000)

        val appCompatCheckedTextView = Espresso.onData(Matchers.anything())
            .inAdapterView(
                allOf(
                    withId(R.id.select_dialog_listview),
                    childAtPosition(
                        withId(R.id.contentPanel),
                        0
                    )
                )
            )
            .atPosition(1)
        appCompatCheckedTextView.perform(click())
        Thread.sleep(2000)

        textInputEditText2.perform(scrollTo(), replaceText("Pogaženi rakun"), closeSoftKeyboard())
        Thread.sleep(2000)

        val materialButton3 = onView(
            allOf(
                withId(R.id.btnSelectPhoto), withText("Select from gallery")
            )
        )
        materialButton3.perform(scrollTo(), click())
        Thread.sleep(7000)

        val materialButton4 = onView(
            allOf(
                withId(R.id.btnSubmit), withText("Submit")

            )
        )
        materialButton4.perform(scrollTo(), click())
        Thread.sleep(5000)

        val viewGroup = onView(
            allOf(
                childAtPosition(
                    allOf(
                        withId(R.id.singleReportRoot),
                        childAtPosition(
                            withId(R.id.rvAllReports),
                            0
                        )
                    ),
                    0
                ),
                isDisplayed()
            )
        )
        viewGroup.check(matches(isDisplayed()))

        val textView = onView(
            allOf(
                withId(R.id.tvTitle), withText("Mrtva životinja")
            )
        )
        textView.check(matches(isDisplayed()))

        Thread.sleep(1000)

        val appCompatImageButton2 = onView(
            allOf(
                withContentDescription("Open navigation drawer"), isDisplayed()
            )
        )
        appCompatImageButton2.perform(click())
        Thread.sleep(3000)

        val numberOfReports = onView(
            allOf(
                withId(R.id.headerTotalReports),
                isDisplayed()
            )
        )
        numberOfReports.check(matches(withText("2")))
        Thread.sleep(1000)
        Espresso.pressBack()
        Thread.sleep(2000)

        val textView1 = onView(
            allOf(
                withId(R.id.tvTitle), withText("Mrtva životinja")
            )
        )
        textView1.perform(click())
        Thread.sleep(2000)

        val materialButton2 = onView(
            allOf(
                withId(R.id.btnDeleteReport), withText("Delete Report"), isDisplayed()
            )
        )
        materialButton2.perform(click())
        Thread.sleep(2000)

        val materialButtonOK = onView(
            allOf(
                withId(android.R.id.button1), withText("OK")
            )
        )
        materialButtonOK.perform(scrollTo(), click())
        Thread.sleep(2000)

        val appCompatImageButton3 = onView(
            allOf(
                withContentDescription("Open navigation drawer"), isDisplayed()
            )
        )
        appCompatImageButton3.perform(click())
        Thread.sleep(3000)

        val numberOfReports2 = onView(
            allOf(
                withId(R.id.headerTotalReports), withText("1")
            )
        )
        numberOfReports2.check(matches(isDisplayed()))
        Thread.sleep(1000)
        TestUtil.signOut(R.id.btnSignOutDrawer)
        Thread.sleep(2000)

    }

    @Test
    fun imageWithoutLocationTest() {

        Thread.sleep(2000)
        TestUtil.login("d10810589@urhen.com", "sifra1234")
        Thread.sleep(2000)

        val floatingActionButton = onView(
            allOf(
                withId(R.id.fabNewReport), isDisplayed()
            )
        )
        floatingActionButton.perform(click())
        Thread.sleep(2000)

        val materialButton2 = onView(
            allOf(
                withId(R.id.btnSelectPhoto), withText("Select from gallery")
            )
        )
        materialButton2.perform(scrollTo(), click())
        Thread.sleep(4000)
        onView(withId(com.google.android.material.R.id.snackbar_text))
            .check(matches(withText("This image is missing location data and cannot be reported")))
        Thread.sleep(2000)

        val appCompatImageButton = onView(
            allOf(
                withContentDescription("Navigate up"), isDisplayed()
            )
        )
        appCompatImageButton.perform(click())
        Thread.sleep(2000)

        val appCompatImageButton2 = onView(
            allOf(
                withContentDescription("Open navigation drawer"), isDisplayed()
            )
        )
        appCompatImageButton2.perform(click())
        Thread.sleep(2000)
        TestUtil.signOut(R.id.btnSignOutDrawer)

    }

}
