package hr.fer.zemris.opp.ekoreport

import android.view.View
import android.view.ViewGroup
import androidx.annotation.IdRes
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.matcher.ViewMatchers.*
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.allOf
import org.hamcrest.TypeSafeMatcher

object TestUtil {

    internal fun login(email: String, password: String) {
        val emailInputEditText = onView(
            allOf(
                withId(R.id.etLoginEmail), isDisplayed()
            )
        )
        emailInputEditText.perform(replaceText(email), closeSoftKeyboard())

        val passwordInputEditText = onView(
            allOf(
                withId(R.id.etLoginPassword), isDisplayed()
            )
        )
        passwordInputEditText.perform(
            replaceText(password),
            closeSoftKeyboard()
        )

        val signInBtn = onView(
            allOf(
                withId(R.id.btnSignIn),
                withText(R.string.sign_in),
                isDisplayed()
            )
        )
        signInBtn.perform(click())
    }

    internal fun signOut(@IdRes id: Int) {
        val materialButton = onView(
            allOf(
                withId(id),
                withText(R.string.sign_out),
                isDisplayed()
            )
        )
        materialButton.perform(click())
    }

    internal fun childAtPosition(
        parentMatcher: Matcher<View>, position: Int
    ): Matcher<View> = object : TypeSafeMatcher<View>() {

        override fun describeTo(description: Description) {
            description.appendText("Child at position $position in parent ")
            parentMatcher.describeTo(description)
        }

        public override fun matchesSafely(view: View): Boolean {
            val parent = view.parent
            return parent is ViewGroup && parentMatcher.matches(parent)
                    && view == parent.getChildAt(position)
        }

    }

}
