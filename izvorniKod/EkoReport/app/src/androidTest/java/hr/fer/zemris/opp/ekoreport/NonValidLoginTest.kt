package hr.fer.zemris.opp.ekoreport

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.closeSoftKeyboard
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import hr.fer.zemris.opp.ekoreport.view.MainActivity
import org.hamcrest.Matchers.allOf
import org.junit.Rule
import org.junit.Test

@LargeTest
class NonValidLoginTest {

    @Rule
    @JvmField
    var mActivityTestRule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun bannedLoginTest() {
        Thread.sleep(3000)

        TestUtil.login("d10811628@urhen.com", "sifra1234")

        Thread.sleep(3000)

        closeSoftKeyboard()

        val textView = onView(
            allOf(
                withId(R.id.tvBannedMessage), isDisplayed()
            )
        )
        textView.check(matches(withText("Test Acc Banned, you are permanently banned for breaking the rules.")))

        TestUtil.signOut(R.id.btnBannedSignOut)
    }

    @Test
    fun unverifiedLoginTest() {
        Thread.sleep(3000)

        TestUtil.login("d10846797@urhen.com", "sifra1234")

        Thread.sleep(3000)

        closeSoftKeyboard()

        val textView2 = onView(
            allOf(
                withId(R.id.tvVerifyEmail), isDisplayed()
            )
        )
        textView2.check(matches(withText(R.string.verify_email_guide)))

        TestUtil.signOut(R.id.btnSignOut)
    }

}
