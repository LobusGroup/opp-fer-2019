package hr.fer.zemris.opp.ekoreport.util

import hr.fer.zemris.opp.ekoreport.util.CheckInputConstraints.checkEmailConstraints
import hr.fer.zemris.opp.ekoreport.util.CheckInputConstraints.checkMandatoryTextConstraints
import hr.fer.zemris.opp.ekoreport.util.CheckInputConstraints.checkPasswordConstraints
import hr.fer.zemris.opp.ekoreport.util.CheckInputConstraints.checkPasswordRepeatConstraints
import hr.fer.zemris.opp.ekoreport.util.CheckInputConstraints.checkPhoneConstraints
import org.junit.Assert
import org.junit.Test

@Suppress("SpellCheckingInspection")
class CheckInputConstraintsTest {

    // PASSWORD

    @Test
    fun checkPasswordConstraintsEmpty() {
        Assert.assertFalse(checkPasswordConstraints("").valid)
    }

    @Test
    fun checkPasswordConstraintsBlankShort() {
        Assert.assertFalse(checkPasswordConstraints("   ").valid)
    }

    @Test
    fun checkPasswordConstraintsBlankLong() {
        Assert.assertTrue(checkPasswordConstraints("      ").valid)
    }

    @Test
    fun checkPasswordConstraintsTooShort() {
        Assert.assertFalse(checkPasswordConstraints("abcde").valid)
    }

    @Test
    fun checkPasswordConstraintsShortestAcceptable() {
        Assert.assertTrue(checkPasswordConstraints("abcde1").valid)
    }

    @Test
    fun checkPasswordConstraintsSpecialSymbols() {
        Assert.assertTrue(checkPasswordConstraints("!#\$%’*+-/=?^_`{|}~").valid)
    }

    @Test
    fun checkPasswordConstraintsJustNumbers() {
        Assert.assertTrue(checkPasswordConstraints("123456789").valid)
    }

    @Test
    fun checkPasswordConstraintsQuotes() {
        Assert.assertTrue(checkPasswordConstraints("abcd\"efg\"").valid)
    }

    @Test
    fun checkPasswordConstraintsTags() {
        Assert.assertTrue(checkPasswordConstraints("abcd<efg>").valid)
    }

    @Test
    fun checkPasswordConstraintsVeryLong() {
        Assert.assertTrue(
            checkPasswordConstraints(
                "abcdefghijabcdefghijabcdefghijabcdefghijabcdefghij" +
                        "abcdefghijabcdefghijabcdefghijabcdefghijabcdefghij"
            ).valid
        )
    }


    // PASSWORD REPEAT

    @Test
    fun checkPasswordRepeatConstraintsBlank() {
        Assert.assertTrue(checkPasswordRepeatConstraints(Pair("  ", "  ")).valid)
    }

    @Test
    fun checkPasswordRepeatConstraintsDifferentBlanks() {
        Assert.assertFalse(checkPasswordRepeatConstraints(Pair(" ", "     ")).valid)
    }

    @Test
    fun checkPasswordRepeatConstraintsEmpty() {
        Assert.assertTrue(checkPasswordRepeatConstraints(Pair("", "")).valid)
    }

    @Test
    fun checkPasswordRepeatConstraintsRegularPassword() {
        Assert.assertTrue(checkPasswordRepeatConstraints(Pair("password", "password")).valid)
    }

    @Test
    fun checkPasswordRepeatConstraintsRegularPasswordForceNewString() {
        Assert.assertTrue(
            checkPasswordRepeatConstraints(
                Pair(
                    String("pass".toCharArray() + "word".toCharArray()),
                    String("pa".toCharArray() + "ssword".toCharArray())
                )
            ).valid
        )
    }

    @Test
    fun checkPasswordRepeatConstraintsUpperCase() {
        Assert.assertFalse(checkPasswordRepeatConstraints(Pair("Password", "password")).valid)
    }

    @Test
    fun checkPasswordRepeatConstraintsAddedSpace() {
        Assert.assertFalse(checkPasswordRepeatConstraints(Pair("  password  ", "password")).valid)
    }


    // EMAIL

    @Test
    fun checkEmailConstraintsEmpty() {
        Assert.assertFalse(checkEmailConstraints("").valid)
    }

    @Test
    fun checkEmailConstraintsBlank() {
        Assert.assertFalse(checkEmailConstraints("   ").valid)
    }

    @Test
    fun checkEmailConstraintsNoAddressSign() {
        Assert.assertFalse(checkEmailConstraints("some.email").valid)
    }

    @Test
    fun checkEmailConstraintsOnlyAddressSign() {
        Assert.assertFalse(checkEmailConstraints("@").valid)
    }

    @Test
    fun checkEmailConstraintsOnlyAddressSignAndLeftSide() {
        Assert.assertFalse(checkEmailConstraints("left@").valid)
    }

    @Test
    fun checkEmailConstraintsOnlyAddressSignAndRightSide() {
        Assert.assertFalse(checkEmailConstraints("@right").valid)
    }

    @Test
    fun checkEmailConstraintsSpecialCharacters() {
        Assert.assertFalse(checkEmailConstraints(".@.").valid)
    }

    @Test
    fun checkEmailConstraintsGarbage() {
        Assert.assertFalse(checkEmailConstraints("#@%^%#\$@#\$@#.com").valid)
    }

    @Test
    fun checkEmailConstraintsEncodedHTML() {
        Assert.assertFalse(checkEmailConstraints("<first.last>@domain.com").valid)
    }

    @Test
    fun checkEmailConstraintsTwoAddressSigns() {
        Assert.assertFalse(checkEmailConstraints("first.last@domain@domain.com").valid)
    }

    @Test
    fun checkEmailConstraintsRegularAddress() {
        Assert.assertTrue(checkEmailConstraints("first.last@domain.com").valid)
    }

    @Test
    fun checkEmailConstraintsRegularAddressWithSymbols() {
        Assert.assertFalse(
            checkEmailConstraints("first!#\$%’*+-/=?^_`{|}~last@domain.com").valid
        )
    }

    @Test
    fun checkEmailConstraintsQuotedLocalPart() {
        Assert.assertTrue(checkEmailConstraints("first?last?@domain.com").valid)
    }

    @Test
    fun checkEmailConstraintsAddressWithSpace() {
        Assert.assertFalse(checkEmailConstraints("first last@domain.com").valid)
    }

    @Test
    fun checkEmailConstraintsQuotedLocalPartInvalidCharacter() {
        Assert.assertFalse(checkEmailConstraints("first?“?last@domain.com").valid)
    }

    @Test
    fun checkEmailConstraintsLeadingDot() {
        Assert.assertFalse(checkEmailConstraints(".first.last@domain.com").valid)
    }

    @Test
    fun checkEmailConstraintsTrailingDot() {
        Assert.assertFalse(checkEmailConstraints("first.last.@domain.com").valid)
    }

    @Test
    fun checkEmailConstraintsMultipleDots() {
        Assert.assertFalse(checkEmailConstraints("first..last@domain.com").valid)
    }

    @Test
    fun checkEmailConstraintsUnicodeCharacters() {
        Assert.assertFalse(checkEmailConstraints("こんにちは@domain.com").valid)
    }

    @Test
    fun checkEmailConstraintsIPAddress() {
        Assert.assertTrue(checkEmailConstraints("first.last@123.123.123.123").valid)
    }

    @Test
    fun checkEmailConstraintsTextFollowedEmail() {
        Assert.assertFalse(checkEmailConstraints("first.last@domain.com (Name)").valid)
    }

    @Test
    fun checkEmailConstraintsMissingTopLevelDomain() {
        Assert.assertFalse(checkEmailConstraints("first.last@domain").valid)
    }

    @Test
    fun checkEmailConstraintsLeadingDashInFrontOfDomain() {
        Assert.assertFalse(checkEmailConstraints("first.last@-domain").valid)
    }

    @Test
    fun checkEmailConstraintsMultipleDotsInDomainName() {
        Assert.assertFalse(checkEmailConstraints("first.last@domain..com").valid)
    }

    @Test
    fun checkEmailConstraintsQuotesAroundAddressField() {
        Assert.assertTrue(checkEmailConstraints("\"first.last\"@domain.com").valid)
    }

    @Test
    fun checkEmailConstraintsRegularAddressPlusSign() {
        Assert.assertTrue(checkEmailConstraints("first+last@domain.com").valid)
    }

    @Test
    fun checkEmailConstraintsDomainWithSubdomain() {
        Assert.assertTrue(checkEmailConstraints("first.last@subdomain.domain.com").valid)
    }

    @Test
    fun checkEmailConstraintsDashInDomainName() {
        Assert.assertTrue(checkEmailConstraints("first.last@domain-name.com").valid)
    }

    @Test
    fun checkEmailConstraintsDashInAddressField() {
        Assert.assertTrue(checkEmailConstraints("first-last@domainname.com").valid)
    }

    @Test
    fun checkEmailConstraintsRegularAddressLeftSideUpperCase() {
        Assert.assertFalse(checkEmailConstraints("FIRST.LAST@domain.com").valid)
    }

    @Test
    fun checkEmailConstraintsRegularAddressRightSideUpperCase() {
        Assert.assertFalse(checkEmailConstraints("first.last@DOMAIN.COM").valid)
    }

    @Test
    fun checkEmailConstraintsRegularAddressInitialsUpperCase() {
        Assert.assertFalse(checkEmailConstraints("First.Last@domain.com").valid)
    }

    @Test
    fun checkEmailConstraintsAddressWithNumbers() {
        Assert.assertTrue(checkEmailConstraints("first123.last@domain.com").valid)
    }

    @Test
    fun checkEmailConstraintsAddressWithOnlyUnderscore() {
        Assert.assertTrue(checkEmailConstraints("__________@domain.com").valid)
    }

    @Test
    fun checkEmailConstraintsAddressWithOnlyNumbers() {
        Assert.assertTrue(checkEmailConstraints("123456789@domain.com").valid)
    }

    @Test
    fun checkEmailConstraintsAddressStartsWithNumber() {
        Assert.assertTrue(checkEmailConstraints("1first.last@domain.com").valid)
    }

    @Test
    fun checkEmailConstraintsLocalPartWithMoreThan64Characters() {
        Assert.assertFalse(
            checkEmailConstraints(
                "abcdefghijabcdefghijabcdefghijabcdefghij" +
                        "abcdefghijabcdefghijabcdefghij@domain.com"
            ).valid
        )
    }

    @Test
    fun checkEmailConstraintsDomainWithMoreThan255Characters() {
        Assert.assertFalse(
            checkEmailConstraints(
                "first.last@abcdefghijabcdefghijabcdefghijabcdefghij" +
                        "abcdefghijabcdefghijabcdefghijabcdefghijabcdefghij" +
                        "abcdefghijabcdefghijabcdefghijabcdefghijabcdefghij" +
                        "abcdefghijabcdefghijabcdefghijabcdefghijabcdefghij" +
                        "abcdefghijabcdefghijabcdefghijabcdefghijabcdefghij" +
                        "abcdefghijabcdefghijabcdefghijabcdefghij.com"
            ).valid
        )
    }


    // MANDATORY TEXT

    @Test
    fun checkMandatoryTextConstraintsEmpty() {
        Assert.assertFalse(checkMandatoryTextConstraints("").valid)
    }

    @Test
    fun checkMandatoryTextConstraintsBlank() {
        Assert.assertFalse(checkMandatoryTextConstraints("   ").valid)
    }

    @Test
    fun checkMandatoryTextConstraintsNotEmpty() {
        Assert.assertTrue(checkMandatoryTextConstraints("abcd").valid)
    }

    @Test
    fun checkMandatoryTextConstraintsSymbols() {
        Assert.assertTrue(checkMandatoryTextConstraints("!#\$%’*+-/=?^_`{|}~").valid)
    }

    @Test
    fun checkMandatoryTextConstraintsUnderscore() {
        Assert.assertTrue(checkMandatoryTextConstraints("________").valid)
    }

    @Test
    fun checkMandatoryTextConstraintsEscaping() {
        Assert.assertTrue(checkMandatoryTextConstraints("abc\\\"").valid)
    }

    @Test
    fun checkMandatoryTextConstraintsTags() {
        Assert.assertTrue(checkMandatoryTextConstraints("abc<efg>").valid)
    }

    @Test
    fun checkMandatoryTextConstraintsUnicodeCharacters() {
        Assert.assertTrue(checkMandatoryTextConstraints("こんにちは").valid)
    }


    // PHONE NUMBER

    @Test
    fun checkPhoneConstraintsEmpty() {
        Assert.assertFalse(checkPhoneConstraints("").valid)
    }

    @Test
    fun checkPhoneConstraintsBlank() {
        Assert.assertFalse(checkPhoneConstraints("  ").valid)
    }

    @Test
    fun checkPhoneConstraintsAreaCodeWithoutBrackets() {
        Assert.assertTrue(checkPhoneConstraints("+385615676789").valid)
    }

    @Test
    fun checkPhoneConstraintsAreaCodeWithBrackets() {
        Assert.assertTrue(checkPhoneConstraints("+(385)915676789").valid)
    }

    @Test
    fun checkPhoneConstraintsAreaCodeWithoutPlus() {
        Assert.assertTrue(checkPhoneConstraints("(385) 915676789").valid)
    }

    @Test
    fun checkPhoneConstraintsAreaCodeWith10Digits() {
        Assert.assertFalse(checkPhoneConstraints("+(3856783656) 915676789").valid)
    }

    @Test
    fun checkPhoneConstraintsNumberWithDashes() {
        Assert.assertTrue(checkPhoneConstraints("+385-615-676-789").valid)
    }

    @Test
    fun checkPhoneConstraintsNumberWithMultipleDashes() {
        Assert.assertTrue(checkPhoneConstraints("+385--615--676--789").valid)
    }

    @Test
    fun checkPhoneConstraintsNumberWithSpaces() {
        Assert.assertTrue(checkPhoneConstraints("+385 615 676 789").valid)
    }

    @Test
    fun checkPhoneConstraintsWithLetters() {
        Assert.assertFalse(checkPhoneConstraints("+385 615 67a 789").valid)
    }

    @Test
    fun checkPhoneConstraintsWithSymbols() {
        Assert.assertFalse(checkPhoneConstraints("+385 615 67@ 789").valid)
    }

    @Test
    fun checkPhoneConstraintsNumberWithDots() {
        Assert.assertTrue(checkPhoneConstraints("+385.615.67.789").valid)
    }

    @Test
    fun checkPhoneConstraintsLeadingDash() {
        Assert.assertFalse(checkPhoneConstraints("-3856158934789").valid)
    }

}
