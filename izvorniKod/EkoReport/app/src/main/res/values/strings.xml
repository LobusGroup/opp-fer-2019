<?xml version="1.0" encoding="utf-8"?>
<resources>
    <string name="app_name">EkoReport</string>

    <string name="gitlab_pages_link" translatable="false">https://lobusgroup.gitlab.io/opp-fer-2019/</string>

    <string name="sign_in">Sign in</string>
    <string name="email">Email</string>
    <string name="password">Password</string>
    <string name="register">Register</string>
    <string name="forgot_password">Forgot Password?</string>
    <string name="important_numbers">Emergency</string>
    <string name="developers">Developers</string>
    <string name="home_page">Home Page</string>
    <string name="settings">Settings</string>

    <string name="initial_loading_text">Loading data</string>
    <string name="credits_title">EkoReport Developers</string>
    <string name="register_title">Create Your Account</string>

    <string name="reset_password_title">Reset Password</string>
    <string name="reset_password_guide_message">Password reset link will be sent to the provided email address.</string>
    <string name="reset_password_send_button">Send</string>
    <string name="reset_password_email_success_formatted">Password reset email was sent to %1$s</string>
    <string name="reset_password_email_failure">Password reset email was not sent</string>

    <string name="unsuccessful_login_message">Incorrect email or password</string>
    <string name="current_user_banned_message_formatted">%1$s, you are permanently banned for breaking the rules.</string>
    <string name="current_user_welcome_login_message_formatted">Welcome back, %1$s!</string>
    <string name="current_user_welcome_register_message_formatted">%1$s, thank you for caring about the environment!</string>
    <string name="current_user_signed_out_message">You have been signed out successfully</string>
    <string name="network_issue_unspecific">No network connection</string>

    <string name="sign_out">Sign out</string>
    <string name="helpful_numbers">Helpful Numbers</string>

    <string name="first_name">First name</string>
    <string name="last_name">Last name</string>
    <string name="phone_number">Phone number</string>
    <string name="password_confirm">Confirm password</string>

    <string name="refresh">Refresh</string>
    <string name="resend_verification_email">Resend</string>
    <string name="verify_email_guide">Please verify your email address.</string>
    <string name="verify_email_notification">Email is not verified</string>
    <string name="verify_email_resend_success">Verification email sent</string>
    <string name="verify_email_resend_failure">Failed to send verification email</string>

    <string name="register_failed_message">Could not complete registration!</string>
    <string name="passwords_not_matching">Repeated password does not match</string>

    <string name="update_phone_success">Phone number successfully updated</string>
    <string name="update_phone_failure">Failed to update phone number</string>
    <string name="update_password_success">Password successfully updated</string>
    <string name="update_password_failure_incorrect_old">Old password is incorrect</string>
    <string name="update_password_failure_unknown">Failed to update password</string>

    <string name="password_constraint_message_length">Password must be at least 6 characters long</string>
    <string name="password_constraint_message_ok">Password meets all constraints</string>
    <string name="email_constraint_message_ok">Email meets all constraints</string>
    <string name="email_constraint_message_invalid">Email format is invalid</string>
    <string name="mandatory_constraint_message_blank">Field cannot be blank</string>
    <string name="mandatory_constraint_message_ok">Field meets all constraints</string>
    <string name="phone_constraint_message_ok">Phone meets all constraints</string>
    <string name="phone_constraint_message_invalid">Phone number format is invalid</string>

    <string name="save_phone_number">Save</string>
    <string name="change_password">Change Password</string>
    <string name="old_password">Old password</string>
    <string name="new_password">New password</string>

    <string name="report_title">Report title</string>
    <string name="report_category_button">Category</string>
    <string name="report_description">Report description</string>
    <string name="report_image">Report image</string>
    <string name="report_submit">Submit</string>
    <string name="report_take_photo">Take a photo</string>

    <string name="unsaved_changes_message">Discard changes?</string>
    <string name="warning_report_assigned">Report has been assigned</string>
    <string name="total_reports">Total reports</string>
    <string name="accepted_reports">Accepted reports</string>
    <string name="assigned_reports">Assigned reports</string>
    <string name="new_report">New Report</string>

    <string name="status_pending">Pending</string>
    <string name="status_assigned">Assigned</string>
    <string name="status_rejected">Rejected</string>
    <string name="status_completed">Completed</string>

    <string name="category">Category</string>
    <string name="description">Description</string>
    <string name="status">Status</string>

    <string name="categories_list_all">All categories</string>
    <string name="categories_filter_title">Filter reports by category</string>
    <string name="categories_chooser_not_chosen">None chosen</string>
    <string name="categories_chooser_title">Choose report category</string>
    <string name="categories_chooser_not_chosen_snackbar">Report category not selected</string>

    <string name="rating_text">%1$.2f%%, total: %2$d</string>
    <string name="no_rating">New user</string>
    <string name="ban_dialog_title">@string/ban_user</string>
    <string name="ban_dialog_message">User will be permanently banned. Continue?</string>
    <string name="reject_dialog">Report will be rejected! Are you sure?</string>
    <string name="report_completed_message">Report completed.</string>
    <string name="report_rejected_message">Report rejected.</string>
    <string name="reassign">Reassign</string>
    <string name="assign">Assign to Worker</string>

    <string name="role_switch_regular">Switch to Regular User</string>
    <string name="role_switch_worker">Switch to Worker</string>
    <string name="role_switch_moderator">Switch to Moderator</string>

    <string name="delete_report_dialog">Permanently delete report?</string>
    <string name="select_from_gallery">Select from gallery</string>

    <string name="request_location_permission">Permission needed to confirm report location. Please allow it.</string>
    <string name="request_gps_enabled">Please enable GPS location to take new photos</string>
    <string name="location_permission_not_granted">Please allow location usage to enable taking photographs</string>
    <string name="image_from_gallery_missing_location">This image is missing location data and cannot be reported</string>
    <string name="share_image_from_gallery_multiple">Unable to report multiple images</string>
    <string name="share_image_from_gallery_incorrect_type">Unsupported file type</string>
    <string name="share_image_from_gallery_offline">Report image cannot be shared offline</string>

    <string name="filter">Filter</string>
    <string name="edit_report">Edit Report</string>
    <string name="unassign">Unassign</string>
    <string name="complete_report">Complete Report</string>
    <string name="reject_report">Reject Report</string>
    <string name="ban_user">Ban User</string>
    <string name="delete_report">Delete Report</string>
    <string name="assigned_to">Assigned to:</string>

    <string name="reporter_details">Reporter details</string>
    <string name="user_rating">User rating:</string>
    <string name="name">Name:</string>
    <string name="e_mail">E-mail:</string>
    <string name="phone">Phone:</string>
    <string name="address">Address:</string>

    <string name="no_reports_available">No reports available</string>
    <string name="password_offline">Can\'t update password while offline!</string>
    <string name="lost_connection">Lost network connection</string>
    <string name="no_network">No network connection</string>
    <string name="report_upload_in_progress">Report is still uploading!</string>

    <string name="title_regular_user">Regular user</string>
    <string name="title_worker">Worker</string>
    <string name="title_moderator">Moderator</string>
    <string name="reports_nice">%1$s, NICE!</string>
</resources>
