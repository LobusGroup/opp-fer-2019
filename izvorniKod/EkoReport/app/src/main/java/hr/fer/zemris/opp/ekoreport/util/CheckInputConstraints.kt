package hr.fer.zemris.opp.ekoreport.util

import hr.fer.zemris.opp.ekoreport.R

object CheckInputConstraints {

    fun checkEmailConstraints(email: String): CustomInputErrors {
        // Regex taken from http://emailregex.com
        val crazyRegex: Regex =
            "(?:[a-z0-9!#\$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#\$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)])"
                .toRegex()

        if (!email.matches(crazyRegex))
            return CustomInputErrors(R.string.email_constraint_message_invalid, false)

        // At this point we are sure email form is at least "something@something"
        val parts: List<String> = email.split("@")

        return if (parts[0].length > 64 || parts[1].length > 255 || email.length > 320)
            CustomInputErrors(R.string.email_constraint_message_invalid, false)
        else
            CustomInputErrors(R.string.email_constraint_message_ok, true)
    }

    fun checkPasswordConstraints(password: String): CustomInputErrors =
        if (password.length < 6)
            CustomInputErrors(R.string.password_constraint_message_length, false)
        else
            CustomInputErrors(R.string.password_constraint_message_ok, true)

    fun checkPasswordRepeatConstraints(passwords: Pair<String, String>): CustomInputErrors =
        if (passwords.first != passwords.second)
            CustomInputErrors(R.string.passwords_not_matching, false)
        else
            CustomInputErrors(R.string.password_constraint_message_ok, true)

    fun checkMandatoryTextConstraints(text: String): CustomInputErrors =
        if (text.isBlank())
            CustomInputErrors(R.string.mandatory_constraint_message_blank, false)
        else
            CustomInputErrors(R.string.mandatory_constraint_message_ok, true)

    fun checkPhoneConstraints(phoneNumber: String): CustomInputErrors {
        val phoneRegex: Regex = "^[+]*[(]?[0-9]{1,4}[)]?[-\\s./0-9]*\$".toRegex()
        return if (phoneNumber.matches(phoneRegex))
            CustomInputErrors(R.string.phone_constraint_message_ok, true)
        else
            CustomInputErrors(R.string.phone_constraint_message_invalid, false)
    }

    // Because it always returns true :)
    private val yoursTruly = CustomInputErrors(0, true)

    // Used for resetting errors with each change
    fun alwaysTrue(@Suppress("UNUSED_PARAMETER") s: String): CustomInputErrors = yoursTruly

}
