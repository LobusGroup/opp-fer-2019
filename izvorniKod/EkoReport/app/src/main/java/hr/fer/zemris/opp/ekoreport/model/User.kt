package hr.fer.zemris.opp.ekoreport.model

import com.google.firebase.firestore.DocumentId
import com.google.firebase.firestore.Exclude

data class User(
    @DocumentId var documentId: String = "",
    val firstName: String = "",
    val lastName: String = "",
    val email: String = "",
    val phone: String = "",
    val totalReports: Int = 0,
    val completedReports: Int = 0,
    val rejectedReports: Int = 0,
    val banned: Boolean = false,
    val FCMtoken: String? = null
) {
    @get:Exclude
    val displayName: String by lazy { "$firstName $lastName" }

    @get:Exclude
    val credible: Boolean by lazy {
        totalReports >= 10
                && completedReports >= 7
                && rejectedReports.toDouble() / completedReports < 0.15
    }
}
