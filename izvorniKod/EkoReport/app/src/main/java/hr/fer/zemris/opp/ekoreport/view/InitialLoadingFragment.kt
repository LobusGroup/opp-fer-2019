package hr.fer.zemris.opp.ekoreport.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.whenResumed
import hr.fer.zemris.opp.ekoreport.R
import kotlinx.android.synthetic.main.fragment_initial_loading.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class InitialLoadingFragment : AuthenticationSensitiveFragment<StatusMessageListener>() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_initial_loading, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            requireActivity().finish()
        }

        viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Main.immediate) {
            whenResumed {
                delay(400L)
                tvLoadingDataIndicator.visibility = View.VISIBLE
                tvLoadingDataIndicatorDots.visibility = View.VISIBLE

                while (true) for (i in 0..3) {
                    tvLoadingDataIndicatorDots.text = ".".repeat(i)
                    delay(400L)
                }
            }
        }
    }

    override fun onAuthenticated() = exitFragment()

}
