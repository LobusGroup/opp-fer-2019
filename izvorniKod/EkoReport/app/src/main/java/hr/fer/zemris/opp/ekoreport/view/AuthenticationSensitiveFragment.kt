package hr.fer.zemris.opp.ekoreport.view

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.firebase.auth.FirebaseAuth
import hr.fer.zemris.opp.ekoreport.R
import hr.fer.zemris.opp.ekoreport.model.ActiveUserViewModel
import hr.fer.zemris.opp.ekoreport.model.ActiveUserViewModel.AuthenticationState.*
import hr.fer.zemris.opp.ekoreport.util.update

abstract class AuthenticationSensitiveFragment<T : StatusMessageListener> : ListenerFragment<T>() {

    protected val activeUserViewModel: ActiveUserViewModel by activityViewModels()
    protected val mAuth by lazy { FirebaseAuth.getInstance() }
    protected val navController by lazy { findNavController() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activeUserViewModel.authenticationState.observe(viewLifecycleOwner) {
            when (it!!) {
                INITIAL_LOADING -> onInitialLoading()
                UNAUTHENTICATED -> onUnauthenticated()
                AUTHENTICATED -> onAuthenticated()
                BANNED -> onBanned()
                EMAIL_NOT_VERIFIED -> onEmailNotVerified()
            }
        }

        activeUserViewModel.currentRole.observe(viewLifecycleOwner) {
            if (it != null) onRoleChanged()
        }
    }

    /** Default implementation does nothing. */
    protected open fun onInitialLoading() = Unit

    /** Default implementation navigates to LoginFragment and clears the back stack. */
    protected open fun onUnauthenticated(): Unit =
        navController.navigate(R.id.action_global_loginFragment)

    /** Default implementation does nothing. */
    protected open fun onAuthenticated() = Unit

    /** Default implementation navigates to BannedFragment and clears the back stack. */
    protected open fun onBanned(): Unit =
        navController.navigate(R.id.action_global_bannedFragment)

    /** Default implementation navigates to VerifyEmailFragment and clears the back stack. */
    protected open fun onEmailNotVerified(): Unit =
        navController.navigate(R.id.action_global_verifyEmailFragment)


    /** Default implementation does nothing. */
    protected open fun onRoleChanged() = Unit


    protected fun showLoginWelcomeMessage() {
        val msg = getString(
            R.string.current_user_welcome_login_message_formatted,
            activeUserViewModel.user.value!!.displayName
        )
        listener?.showSnackbar(msg)
    }

    protected fun showRegisterWelcomeMessage() {
        val msg = getString(
            R.string.current_user_welcome_register_message_formatted,
            activeUserViewModel.user.value!!.displayName
        )
        listener?.showSnackbar(msg)
    }


    /** Shows a confirmation dialog before leaving the fragment. */
    protected open fun showConfirmationDialog(): AlertDialog =
        MaterialAlertDialogBuilder(requireContext())
            .setTitle(R.string.unsaved_changes_message)
            .setPositiveButton(android.R.string.yes) { _, _ -> exitFragment() }
            .setNegativeButton(android.R.string.no, null)
            .show()

    protected open fun exitFragment() {
        // Just in case...
        activeUserViewModel.currentlyLoading.update(false)
        navController.navigateUp()
    }


    private companion object {
        // Do not use class.java.simpleName because it is too long
        @Suppress("unused")
        private const val TAG = "AuthenticationSensitive"
    }

}
