package hr.fer.zemris.opp.ekoreport.model

import com.google.firebase.firestore.DocumentId
import com.google.firebase.firestore.Exclude

data class Category(
    @DocumentId val documentId: String = "",
    val titleEN: String = "",
    val titleHR: String = "",
    @Exclude var titleLocal: String = ""
) {
    @get:Exclude
    val categoryId: Int by lazy { documentId.toInt() }
}
