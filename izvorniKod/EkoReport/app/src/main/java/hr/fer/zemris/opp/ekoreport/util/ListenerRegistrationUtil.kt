package hr.fer.zemris.opp.ekoreport.util

import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.EventListener
import com.google.firebase.firestore.ListenerRegistration
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlin.reflect.KMutableProperty0

internal fun attachListener(
    collectionPath: String,
    documentPath: String,
    listener: EventListener<DocumentSnapshot>
): ListenerRegistration =
    Firebase.firestore
        .collection(collectionPath)
        .document(documentPath)
        .addSnapshotListener(listener)

internal fun detachListener(l: KMutableProperty0<ListenerRegistration?>) {
    l.get()?.remove()
    l.set(null)
}
