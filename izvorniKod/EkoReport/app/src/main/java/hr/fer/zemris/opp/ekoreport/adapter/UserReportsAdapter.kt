package hr.fer.zemris.opp.ekoreport.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.NavController
import androidx.recyclerview.widget.RecyclerView
import com.firebase.ui.firestore.FirestoreRecyclerAdapter
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import hr.fer.zemris.opp.ekoreport.R
import hr.fer.zemris.opp.ekoreport.model.ActiveReportViewModel
import hr.fer.zemris.opp.ekoreport.model.ActiveUserViewModel
import hr.fer.zemris.opp.ekoreport.model.Report
import hr.fer.zemris.opp.ekoreport.model.Report.Companion.statusCodeToStringRes
import hr.fer.zemris.opp.ekoreport.util.loadImageRemote
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.fragment_home_page.*
import kotlinx.android.synthetic.main.report_single.view.*

class UserReportsAdapter(
    options: FirestoreRecyclerOptions<Report>,
    private val caller: Fragment,
    private val navController: NavController
) : FirestoreRecyclerAdapter<Report, UserReportsAdapter.ViewHolder>(options) {

    private val mStorage by lazy { Firebase.storage }
    private val activeReportViewModel: ActiveReportViewModel by caller.activityViewModels()
    private val activeUserViewModel: ActiveUserViewModel by caller.activityViewModels()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.report_single, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int, item: Report) {
        holder.apply {
            tvTitle.text = item.title
            tvCategoryLocal.text = activeUserViewModel.categoriesMap!!
                .getValue(item.category).titleLocal
            tvAddress.text = item.addressShort
            tvStatus.text = tvStatus.context.getString(statusCodeToStringRes(item.statusCode))

            singleReportRoot.setOnClickListener {
                activeReportViewModel.setActiveReport(item)
                navController.navigate(R.id.action_homePageFragment_to_reportDetailsFragment)
            }

            val photoReference = mStorage.getReference(item.imageReport)
            loadImageRemote(photoReference, ivPhoto, true)
        }
    }

    override fun onDataChanged() {
        super.onDataChanged()
        caller.tvNoReports.visibility = if (itemCount == 0) View.VISIBLE else View.GONE
    }

    inner class ViewHolder(override val containerView: View) :
        RecyclerView.ViewHolder(containerView),
        LayoutContainer {

        val singleReportRoot: ViewGroup = containerView.singleReportRoot
        val ivPhoto: ImageView = containerView.ivPhoto
        val tvTitle: TextView = containerView.tvTitle
        val tvCategoryLocal: TextView = containerView.tvCategoryLocal
        val tvAddress: TextView = containerView.tvAddress
        val tvStatus: TextView = containerView.tvStatus
    }

}
