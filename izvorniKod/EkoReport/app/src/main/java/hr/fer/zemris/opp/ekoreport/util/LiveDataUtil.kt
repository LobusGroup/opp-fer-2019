@file:Suppress("unused")

package hr.fer.zemris.opp.ekoreport.util

import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

internal fun <T> MutableLiveData<T>.update(
    newValue: T,
    reassignDuplicate: Boolean = false
) {
    if (reassignDuplicate || value != newValue)
        value = newValue
}

internal suspend fun <T> MutableLiveData<T>.safeUpdate(
    newValue: T,
    reassignDuplicate: Boolean = false
) = withContext(Dispatchers.Main) {
    update(newValue, reassignDuplicate)
}

internal fun <T> MutableLiveData<MutableSet<T>>.perform(
    newValue: T,
    action: MutableSet<T>.(T) -> Boolean
): Boolean = this.value!!.action(newValue).also {
    if (it) notifyObservers()
}

internal suspend fun <T> MutableLiveData<MutableSet<T>>.safePerform(
    newValue: T,
    action: MutableSet<T>.(T) -> Boolean
): Boolean = withContext(Dispatchers.Main) { perform(newValue, action) }

internal fun <T> MutableLiveData<MutableSet<T>>.customAdd(value: T): Boolean =
    perform(value, MutableSet<T>::add)

internal fun <T> MutableLiveData<MutableSet<T>>.customRemove(value: T): Boolean =
    perform(value, MutableSet<T>::remove)

internal suspend fun <T> MutableLiveData<MutableSet<T>>.safeAdd(value: T): Boolean =
    safePerform(value, MutableSet<T>::add)

internal suspend fun <T> MutableLiveData<MutableSet<T>>.safeRemove(value: T): Boolean =
    safePerform(value, MutableSet<T>::remove)

internal suspend fun <T> MutableLiveData<MutableSet<T>>.safeClear() =
    withContext(Dispatchers.Main) { value?.clear() }

internal fun MutableLiveData<*>.notifyObservers() {
    value = value
}

internal suspend fun MutableLiveData<*>.safeNotifyObservers() =
    withContext(Dispatchers.Main) { notifyObservers() }
