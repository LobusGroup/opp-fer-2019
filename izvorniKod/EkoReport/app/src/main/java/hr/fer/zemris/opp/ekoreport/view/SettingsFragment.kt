package hr.fer.zemris.opp.ekoreport.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import hr.fer.zemris.opp.ekoreport.R
import hr.fer.zemris.opp.ekoreport.model.NetworkStateViewModel.Companion.isOnline
import hr.fer.zemris.opp.ekoreport.util.CheckInputConstraints.checkPasswordConstraints
import hr.fer.zemris.opp.ekoreport.util.CheckInputConstraints.checkPasswordRepeatConstraints
import hr.fer.zemris.opp.ekoreport.util.CheckInputConstraints.checkPhoneConstraints
import hr.fer.zemris.opp.ekoreport.util.CustomTextWatcher.Companion.addCustomTextChangedListener
import hr.fer.zemris.opp.ekoreport.util.ErrorCollator
import kotlinx.android.synthetic.main.fragment_settings.*

class SettingsFragment : AuthenticationSensitiveFragment<StatusMessageListener>() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_settings, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        etSettingsPhoneNumber.setText(activeUserViewModel.user.value!!.phone)

        btnSavePhoneNumber.setOnClickListener { onUpdateNumber() }
        btnSavePassword.setOnClickListener { onUpdatePassword() }

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            if (hasUnsavedChanges()) showConfirmationDialog()
            else exitFragment()
        }

        addCustomTextChangedListener(
            etSettingsPhoneNumber,
            tilSettingsPhoneNumber,
            ::checkPhoneConstraints
        )
        addCustomTextChangedListener(
            etSettingsOldPassword,
            tilSettingsOldPassword,
            ::checkPasswordConstraints
        )
        addCustomTextChangedListener(
            etSettingsPasswordFirst,
            tilSettingsPasswordFirst,
            ::checkPasswordConstraints
        )
        addCustomTextChangedListener(
            etSettingsPasswordSecond,
            tilSettingsPasswordSecond,
            ::checkPasswordConstraints
        )
    }

    private fun onUpdateNumber() {
        val phoneNumber = etSettingsPhoneNumber.text.toString().trim()

        with(ErrorCollator(resources)) {
            check(phoneNumber, tilSettingsPhoneNumber, ::checkPhoneConstraints)

            if (errorDetected)
                return
        }

        activeUserViewModel.updatePhoneNumber(phoneNumber)
    }

    private fun onUpdatePassword() {
        if (!isOnline(requireContext())) {
            listener?.showSnackbar(R.string.password_offline)
            return
        }

        // Do NOT trim passwords
        val oldPassword = etSettingsOldPassword.text.toString()
        val passwordFirst = etSettingsPasswordFirst.text.toString()
        val passwordSecond = etSettingsPasswordSecond.text.toString()

        with(ErrorCollator(resources)) {
            check(oldPassword, tilSettingsOldPassword, ::checkPasswordConstraints)
            check(passwordFirst, tilSettingsPasswordFirst, ::checkPasswordConstraints)
            check(
                Pair(passwordFirst, passwordSecond),
                tilSettingsPasswordSecond,
                ::checkPasswordRepeatConstraints
            )

            if (errorDetected)
                return
        }

        activeUserViewModel.updatePassword(oldPassword, passwordFirst)
    }

    // Password changes not included since they are rather hard to track.
    private fun hasUnsavedChanges(): Boolean =
        etSettingsPhoneNumber.text.toString().trim() != activeUserViewModel.user.value!!.phone

}
