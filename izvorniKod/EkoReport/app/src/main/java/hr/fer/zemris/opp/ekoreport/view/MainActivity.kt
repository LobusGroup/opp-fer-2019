package hr.fer.zemris.opp.ekoreport.view

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.core.view.updatePadding
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.observe
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import hr.fer.zemris.opp.ekoreport.R
import hr.fer.zemris.opp.ekoreport.model.ActiveReportViewModel
import hr.fer.zemris.opp.ekoreport.model.ActiveUserViewModel
import hr.fer.zemris.opp.ekoreport.model.ActiveUserViewModel.AuthenticationState.UNAUTHENTICATED
import hr.fer.zemris.opp.ekoreport.util.fromDpToPx
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity :
    AppCompatActivity(),
    StatusMessageListener,
    HomePageFragment.FloatingButtonListener {

    private val activeUserViewModel: ActiveUserViewModel by viewModels()
    private val activeReportViewModel: ActiveReportViewModel by viewModels()
    private val navController: NavController by lazy { findNavController(R.id.nav_host_fragment) }

    private val topLevelFragments = setOf(
        R.id.homePageFragment,
        R.id.loginFragment,
        R.id.verifyEmailFragment,
        R.id.bannedFragment
    )

    private val noToolbarFragments = listOf(
        R.id.loginFragment,
        R.id.registerFragment,
        R.id.initialLoadingFragment,
        R.id.bannedFragment
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)  // must be called before super.onCreate
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        fun toolbarStatusDrawerUpdate(b: Boolean) {
            activeUserViewModel.forceDrawerLock.value = !b
            if (b) {
                toolbar.visibility = VISIBLE
                window.statusBarColor = getColor(R.color.statusBarColor)
            } else {
                toolbar.visibility = GONE
                window.statusBarColor = getColor(R.color.colorBackground)
            }
        }

        navController.addOnDestinationChangedListener { _, destination, _ ->
            toolbarStatusDrawerUpdate(
                destination.id !in noToolbarFragments
                        && activeUserViewModel.authenticationState.value != UNAUTHENTICATED
            )

            if (destination.id == R.id.homePageFragment) {
                activeUserViewModel.filterIconVisible.value = true
                fabNewReport.show()
            } else {
                activeUserViewModel.filterIconVisible.value = false
                fabNewReport.hide()
            }
        }

        val drawerLocked = MediatorLiveData<Boolean>()
        drawerLocked.addSource(activeUserViewModel.forceDrawerLock) {
            drawerLocked.setValue(it || activeUserViewModel.currentlyLoading.value!!)
        }
        drawerLocked.addSource(activeUserViewModel.currentlyLoading) {
            drawerLocked.setValue(it || activeUserViewModel.forceDrawerLock.value!!)
        }
        drawerLocked.observe(this) {
            layout_root.setDrawerLockMode(
                if (it) DrawerLayout.LOCK_MODE_LOCKED_CLOSED
                else DrawerLayout.LOCK_MODE_UNLOCKED
            )
        }

        activeUserViewModel.authenticationState.observe(this) {
            if (it == UNAUTHENTICATED)
                toolbarStatusDrawerUpdate(false)
        }

        activeUserViewModel.currentlyLoading.observe(this) {
            loading_indicator.visibility = if (it) VISIBLE else GONE
        }

        activeUserViewModel.filterIconVisible.observe(this) { invalidateOptionsMenu() }

        val appBarConfiguration = AppBarConfiguration(
            topLevelFragments, layout_root
        )
        nav_view.setupWithNavController(navController)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        setupActionBarWithNavController(navController, appBarConfiguration)

        layout_root.addDrawerListener(NavDrawerListener(activeUserViewModel, layout_root))

        activeUserViewModel.notificationChannel.observe(this) {
            it?.let {
                if (it.formatArgs == null) showSnackbar(it.resId)
                else showSnackbar(getString(it.resId, it.formatArgs))

                activeUserViewModel.notificationChannel.value = null
            }
        }

        when (intent?.action) {
            Intent.ACTION_SEND -> {
                if (intent.type?.startsWith("image/") == true) {
                    val imageUri = intent.getParcelableExtra(Intent.EXTRA_STREAM) as? Uri
                    if (imageUri != null) activeReportViewModel.shareImage(imageUri)
                } else {
                    showSnackbar(R.string.share_image_from_gallery_incorrect_type)
                }
            }
            Intent.ACTION_SEND_MULTIPLE -> showSnackbar(R.string.share_image_from_gallery_multiple)
            else -> Unit
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        val visible = activeUserViewModel.filterIconVisible.value!!
        menu?.findItem(R.id.action_filter)?.isVisible = visible
        ivToolbarLeaf.updatePadding(right = resources.fromDpToPx(if (visible) 24f else 72f))
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            if (navController.currentDestination!!.id in topLevelFragments)
                layout_root.openDrawer(GravityCompat.START)
            else
                onBackPressedDispatcher.onBackPressed()
            return true
        }

        if (item.itemId == R.id.action_filter)
            activeUserViewModel.filterRequested.value = true
        return super.onOptionsItemSelected(item)
    }

    override fun onSupportNavigateUp(): Boolean =
        navController.navigateUp() || super.onSupportNavigateUp()

    override fun onBackPressed() {
        if (layout_root.isDrawerVisible(GravityCompat.START))
            layout_root.closeDrawer(GravityCompat.START)
        else
            super.onBackPressed()
    }

    override fun getFAB(): FloatingActionButton = fabNewReport

    override fun showToast(resId: Int, duration: Int) =
        Toast.makeText(this, resId, duration).show()

    override fun showToast(text: CharSequence, duration: Int) =
        Toast.makeText(this, text, duration).show()

    override fun showSnackbar(
        resId: Int,
        duration: Int,
        showImmediately: Boolean
    ): Snackbar = Snackbar.make(layout_coordinator, resId, duration).apply {
        if (showImmediately) show()
    }

    override fun showSnackbar(
        text: CharSequence,
        duration: Int,
        showImmediately: Boolean
    ): Snackbar = Snackbar.make(layout_coordinator, text, duration).apply {
        if (showImmediately) show()
    }

}
