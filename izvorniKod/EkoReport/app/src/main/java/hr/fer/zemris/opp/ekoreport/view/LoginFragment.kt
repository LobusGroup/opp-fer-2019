package hr.fer.zemris.opp.ekoreport.view

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import hr.fer.zemris.opp.ekoreport.R
import hr.fer.zemris.opp.ekoreport.util.CheckInputConstraints
import hr.fer.zemris.opp.ekoreport.util.CheckInputConstraints.checkEmailConstraints
import hr.fer.zemris.opp.ekoreport.util.CheckInputConstraints.checkPasswordConstraints
import hr.fer.zemris.opp.ekoreport.util.CustomTextWatcher.Companion.addCustomTextChangedListener
import hr.fer.zemris.opp.ekoreport.util.ErrorCollator
import kotlinx.android.synthetic.main.dialog_reset_password.view.*
import kotlinx.android.synthetic.main.fragment_login.*

class LoginFragment : AuthenticationSensitiveFragment<StatusMessageListener>() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_login, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnSignIn.setOnClickListener { signIn() }
        btnRegister.setOnClickListener {
            navController.navigate(R.id.action_loginFragment_to_registerFragment)
        }
        btnResetPassword.setOnClickListener { resetPassword() }

        btnLoginPhoneNumbers.setOnClickListener {
            navController.navigate(R.id.action_loginFragment_to_phoneNumbersFragment)
        }
        btnLoginDevCredits.setOnClickListener {
            navController.navigate(R.id.action_loginFragment_to_developerCreditsFragment)
        }

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            requireActivity().finish()
        }

        addCustomTextChangedListener(
            etLoginEmail,
            tilLoginEmail,
            CheckInputConstraints::alwaysTrue
        )
        addCustomTextChangedListener(
            etLoginPassword,
            tilLoginPassword,
            CheckInputConstraints::alwaysTrue
        )
    }

    override fun onUnauthenticated() = Unit

    override fun onAuthenticated() {
        showLoginWelcomeMessage()
        exitFragment()
    }

    private fun signIn() {
        val email = etLoginEmail.text.toString()
        val password = etLoginPassword.text.toString()

        with(ErrorCollator(resources)) {
            check(email, tilLoginEmail, ::checkEmailConstraints)
            check(password, tilLoginPassword, ::checkPasswordConstraints)

            if (errorDetected)
                return
        }

        activeUserViewModel.authenticate(email, password)
    }

    private fun resetPassword() {
        val dialogBuilder = MaterialAlertDialogBuilder(requireContext())
            .setTitle(R.string.reset_password_title)
            .setMessage(R.string.reset_password_guide_message)

        val v = LayoutInflater
            .from(dialogBuilder.context)
            .inflate(
                R.layout.dialog_reset_password,
                fragment_login_root_viewgroup,
                false
            )

        dialogBuilder
            .setView(v)
            .setPositiveButton(R.string.reset_password_send_button) { _, _ ->
                passwordResetPositiveButton(v.etPasswordResetEmail.text.toString().trim())
            }.setNegativeButton(android.R.string.cancel, null)
            .show()
    }

    @SuppressLint("StringFormatInvalid")
    private fun passwordResetPositiveButton(email: String) {
        if (!checkEmailConstraints(email).valid) {
            listener?.showSnackbar(R.string.reset_password_email_failure)
            return
        }

        mAuth.sendPasswordResetEmail(email)
        val msg = getString(R.string.reset_password_email_success_formatted, email)
        listener?.showSnackbar(msg)
    }

}
