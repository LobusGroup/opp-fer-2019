package hr.fer.zemris.opp.ekoreport.model

import com.google.firebase.firestore.DocumentId

data class PhoneNumber(
    @DocumentId val documentId: String = "",
    val nameEN: String = "",
    val nameHR: String = "",
    val phone: String = "",
    val emergency: Boolean = false
)
