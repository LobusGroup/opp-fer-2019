package hr.fer.zemris.opp.ekoreport.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.ktx.Firebase
import hr.fer.zemris.opp.ekoreport.R
import hr.fer.zemris.opp.ekoreport.adapter.AvailableWorkerAdapter
import hr.fer.zemris.opp.ekoreport.model.ActiveReportViewModel
import hr.fer.zemris.opp.ekoreport.model.Worker
import kotlinx.android.synthetic.main.fragment_dialog_workers.*

class AvailableWorkersDialog : DialogFragment() {

    private val activeReportViewModel: ActiveReportViewModel by activityViewModels()
    private val mFirestore by lazy { Firebase.firestore }
    private lateinit var adapter: AvailableWorkerAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_dialog_workers, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val query = mFirestore
            .collection("workers")
            .whereArrayContains("categories", activeReportViewModel.report.value!!.category)
            .orderBy("liveAssigned", Query.Direction.ASCENDING)

        val options = FirestoreRecyclerOptions.Builder<Worker>()
            .setQuery(query) { it.toObject<Worker>()!! }
            .setLifecycleOwner(viewLifecycleOwner)
            .build()

        adapter = AvailableWorkerAdapter(options, this)
        rvAvailableWorkers.layoutManager = LinearLayoutManager(requireContext())
        rvAvailableWorkers.adapter = adapter
        adapter.startListening()
    }

    override fun onDestroyView() {
        super.onDestroyView()

        // Should be done automatically, but we do it explicitly to have cleaner code
        adapter.stopListening()
    }

}
