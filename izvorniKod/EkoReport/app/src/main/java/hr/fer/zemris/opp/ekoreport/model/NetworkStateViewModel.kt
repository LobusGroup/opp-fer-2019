package hr.fer.zemris.opp.ekoreport.model

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import hr.fer.zemris.opp.ekoreport.model.NetworkState.*
import hr.fer.zemris.opp.ekoreport.util.update
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class NetworkStateViewModel : ViewModel() {

    private var connectivityManager: ConnectivityManager? = null
    val networkAvailable by lazy { MutableLiveData<NetworkState>(NOT_MONITORED) }

    fun startMonitoringNetwork(context: Context) {
        networkAvailable.update(
            if (isOnline(context)) AVAILABLE else UNAVAILABLE
        )

        val networkRequest = NetworkRequest.Builder()
            .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
            .addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)
            .build()

        connectivityManager = getConnectivityManager(context.applicationContext)
        connectivityManager!!.registerNetworkCallback(networkRequest, networkCallback)
    }

    fun stopMonitoringNetwork() {
        connectivityManager?.unregisterNetworkCallback(networkCallback)
        networkAvailable.value = NOT_MONITORED
    }

    private val networkCallback = object : ConnectivityManager.NetworkCallback() {
        override fun onLost(network: Network?) = updateState(LOST)
        override fun onUnavailable() = updateState(UNAVAILABLE)
        override fun onLosing(network: Network?, maxMsToLive: Int) = updateState(LOSING)
        override fun onAvailable(network: Network?) = updateState(AVAILABLE)
        fun updateState(state: NetworkState) {
            // Run in GlobalScope on main thread because this callback is triggered in the background
            GlobalScope.launch(Dispatchers.Main) { networkAvailable.update(state) }
        }
    }

    companion object {

        fun getConnectivityManager(context: Context) =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        fun isOnline(context: Context): Boolean {
            val cm = getConnectivityManager(context)

            val n = cm.activeNetwork ?: return false
            val nc = cm.getNetworkCapabilities(n) ?: return false
            // Check both for Wi-Fi and Cellular network
            return nc.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)
                    || nc.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)
                    || nc.hasTransport(NetworkCapabilities.TRANSPORT_VPN)
        }

    }

}
