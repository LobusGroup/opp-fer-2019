package hr.fer.zemris.opp.ekoreport.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import hr.fer.zemris.opp.ekoreport.R
import hr.fer.zemris.opp.ekoreport.model.User
import hr.fer.zemris.opp.ekoreport.util.CheckInputConstraints
import hr.fer.zemris.opp.ekoreport.util.CheckInputConstraints.checkEmailConstraints
import hr.fer.zemris.opp.ekoreport.util.CheckInputConstraints.checkMandatoryTextConstraints
import hr.fer.zemris.opp.ekoreport.util.CheckInputConstraints.checkPasswordConstraints
import hr.fer.zemris.opp.ekoreport.util.CheckInputConstraints.checkPasswordRepeatConstraints
import hr.fer.zemris.opp.ekoreport.util.CheckInputConstraints.checkPhoneConstraints
import hr.fer.zemris.opp.ekoreport.util.CustomTextWatcher.Companion.addCustomTextChangedListener
import hr.fer.zemris.opp.ekoreport.util.ErrorCollator
import kotlinx.android.synthetic.main.fragment_register.*

class RegisterFragment : AuthenticationSensitiveFragment<StatusMessageListener>() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_register, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnRegister.setOnClickListener { registerButtonPressed() }
        btnRegisterBack.setOnClickListener { exitFragment() }

        addCustomTextChangedListener(
            etRegisterFirstName,
            tilRegisterFirstName,
            CheckInputConstraints::checkMandatoryTextConstraints
        )
        addCustomTextChangedListener(
            etRegisterLastName,
            tilRegisterLastName,
            CheckInputConstraints::checkMandatoryTextConstraints
        )
        addCustomTextChangedListener(
            etRegisterEmail,
            tilRegisterEmail,
            CheckInputConstraints::checkEmailConstraints
        )
        addCustomTextChangedListener(
            etRegisterPhone,
            tilRegisterPhone,
            CheckInputConstraints::checkPhoneConstraints
        )
        addCustomTextChangedListener(
            etRegisterPasswordFirst,
            tilRegisterPasswordFirst,
            CheckInputConstraints::checkPasswordConstraints
        )
        addCustomTextChangedListener(
            etRegisterPasswordSecond,
            tilRegisterPasswordSecond,
            CheckInputConstraints::checkPasswordConstraints
        )
    }

    override fun onUnauthenticated() = Unit

    private fun registerButtonPressed() {

        val firstName = etRegisterFirstName.text.toString().trim()
        val lastName = etRegisterLastName.text.toString().trim()
        val email = etRegisterEmail.text.toString().trim()
        val phone = etRegisterPhone.text.toString().trim()

        // Do NOT trim passwords
        val passwordFirst = etRegisterPasswordFirst.text.toString()
        val passwordSecond = etRegisterPasswordSecond.text.toString()

        with(ErrorCollator(resources)) {
            check(firstName, tilRegisterFirstName, ::checkMandatoryTextConstraints)
            check(lastName, tilRegisterLastName, ::checkMandatoryTextConstraints)
            check(email, tilRegisterEmail, ::checkEmailConstraints)
            check(phone, tilRegisterPhone, ::checkPhoneConstraints)
            check(passwordFirst, tilRegisterPasswordFirst, ::checkPasswordConstraints)
            check(
                Pair(passwordFirst, passwordSecond),
                tilRegisterPasswordSecond,
                ::checkPasswordRepeatConstraints
            )

            if (errorDetected)
                return
        }

        activeUserViewModel.register(
            User(firstName = firstName, lastName = lastName, email = email, phone = phone),
            passwordFirst
        )
    }

}
