package hr.fer.zemris.opp.ekoreport.view

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import hr.fer.zemris.opp.ekoreport.R
import hr.fer.zemris.opp.ekoreport.model.ActiveUserViewModel
import hr.fer.zemris.opp.ekoreport.model.Developer
import kotlinx.android.synthetic.main.developer_credits_single_line.view.*
import kotlinx.android.synthetic.main.fragment_developer_credits.*

class DeveloperCreditsFragment : Fragment() {

    private val activeUserViewModel: ActiveUserViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_developer_credits, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(activeUserViewModel.developers) {
            observe(viewLifecycleOwner, Observer {
                (value!!.first.asSequence() + value!!.second.asSequence())
                    .forEach { linearLayoutDevs.addView(createDeveloperView(it)) }
            })
        }

        ivLogo2.setOnClickListener {
            startActivity(Intent().apply {
                action = Intent.ACTION_VIEW
                addCategory(Intent.CATEGORY_BROWSABLE)
                data = Uri.parse(getString(R.string.gitlab_pages_link))
            })
        }
    }

    private fun createDeveloperView(dev: Developer): View {

        val view = LayoutInflater.from(context).inflate(
            R.layout.developer_credits_single_line,
            linearLayoutDevs,
            false
        )

        @SuppressLint("SetTextI18n")
        view.tvFullName.text = "${dev.firstName ?: ""} ${dev.lastName ?: ""}"

        if (dev.linkedin == null) {
            view.imLinkedin.visibility = View.INVISIBLE
        } else {
            view.isClickable = true
            view.isFocusable = true
            view.setOnClickListener {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(dev.linkedin)))
            }
        }

        return view
    }

}
