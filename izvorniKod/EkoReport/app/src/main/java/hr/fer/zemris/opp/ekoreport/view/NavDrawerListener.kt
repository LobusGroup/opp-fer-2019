package hr.fer.zemris.opp.ekoreport.view

import android.content.Context
import android.view.View
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.Observer
import hr.fer.zemris.opp.ekoreport.R
import hr.fer.zemris.opp.ekoreport.model.ActiveUserViewModel
import hr.fer.zemris.opp.ekoreport.model.Role
import hr.fer.zemris.opp.ekoreport.model.Role.*
import hr.fer.zemris.opp.ekoreport.model.User
import hr.fer.zemris.opp.ekoreport.model.Worker
import kotlinx.android.synthetic.main.header_layout.view.*

class NavDrawerListener(
    val activeUserViewModel: ActiveUserViewModel,
    private val drawerLayout: DrawerLayout
) : DrawerLayout.DrawerListener {

    private var userObserver: Observer<in User?>? = null
    private var workerObserver: Observer<in Worker?>? = null

    override fun onDrawerSlide(drawerView: View, slideOffset: Float) = Unit

    override fun onDrawerOpened(drawerView: View) {

        fun onRoleClick(newRole: Role) {
            activeUserViewModel.currentRole.value = newRole
            updateRole(drawerView)
            drawerLayout.closeDrawers()
        }

        // Show role changing buttons
        updateRole(drawerView)

        drawerView.btnRoleRegular.setOnClickListener { onRoleClick(REGULAR_USER) }
        drawerView.btnRoleWorker.setOnClickListener { onRoleClick(WORKER) }
        drawerView.btnRoleModerator.setOnClickListener { onRoleClick(MODERATOR) }

        drawerView.btnSignOutDrawer.setOnClickListener {
            activeUserViewModel.signOut()
            drawerLayout.closeDrawers()
        }
    }

    override fun onDrawerClosed(drawerView: View) = clearObservers()

    override fun onDrawerStateChanged(newState: Int) = Unit

    private fun updateRole(drawerView: View) {

        fun calculateVisibility(role: Role): Int {
            val b = activeUserViewModel.availableRoles.value!!.contains(role)
                    && activeUserViewModel.currentRole.value != role
            return if (b) View.VISIBLE else View.GONE
        }

        drawerView.btnRoleRegular.visibility = calculateVisibility(REGULAR_USER)
        drawerView.btnRoleWorker.visibility = calculateVisibility(WORKER)
        drawerView.btnRoleModerator.visibility = calculateVisibility(MODERATOR)

        drawerView.tvHeaderCurrentRole.visibility = View.VISIBLE

        clearObservers()

        val newPref = when (activeUserViewModel.currentRole.value) {
            REGULAR_USER -> {
                drawerView.tvHeaderCurrentRole.text =
                    drawerLayout.context.getString(R.string.title_regular_user)
                if (activeUserViewModel.availableRoles.value!!.size == 1)
                    drawerView.tvHeaderCurrentRole.visibility = View.GONE
                setTextVisibility(drawerView, View.VISIBLE)
                userObserver = generateUserObserver(drawerView)
                activeUserViewModel.user.observeForever(userObserver!!)
                drawerLayout.context.getString(R.string.preference_mode_regular_user)
            }

            WORKER -> {
                drawerView.tvHeaderCurrentRole.text =
                    drawerLayout.context.getString(R.string.title_worker)
                setTextVisibility(drawerView, View.VISIBLE)
                workerObserver = generateWorkerObserver(drawerView)
                activeUserViewModel.worker.observeForever(workerObserver!!)
                drawerLayout.context.getString(R.string.preference_mode_worker)
            }

            MODERATOR -> {
                drawerView.tvHeaderCurrentRole.text =
                    drawerLayout.context.getString(R.string.title_moderator)
                setTextVisibility(drawerView, View.GONE)
                drawerView.tvHeaderFullName.text = activeUserViewModel.user.value!!.displayName
                drawerLayout.context.getString(R.string.preference_mode_moderator)
            }

            else -> null
        }

        if (newPref != null) {
            val sharedPref = drawerLayout.context.getSharedPreferences(
                drawerLayout.context.getString(R.string.app_name), Context.MODE_PRIVATE
            )

            with(sharedPref.edit()) {
                putString(
                    drawerLayout.context.getString(R.string.preference_preferred_mode),
                    newPref
                )
                apply()
            }
        }
    }

    private fun setTextVisibility(drawerView: View, visibility: Int) {
        drawerView.headerTotalReportsText.visibility = visibility
        drawerView.headerTotalReports.visibility = visibility
        drawerView.headerAcceptedReportsText.visibility = visibility
        drawerView.headerAcceptedReports.visibility = visibility
        drawerView.headerDivider1.visibility = visibility
        drawerView.headerDivider2.visibility = visibility
    }

    private fun generateUserObserver(drawerView: View): Observer<in User?> = Observer {
        it ?: return@Observer
        drawerView.tvHeaderFullName.text = it.displayName
        drawerView.headerTotalReports.text = it.totalReports.toString()
        if (it.totalReports == 69) {
            drawerView.headerTotalReports.text = drawerView.context.getString(
                R.string.reports_nice,
                drawerView.headerTotalReports.text.toString()
            )
        }
        drawerView.headerAcceptedReportsText.text =
            drawerView.context.getString(R.string.accepted_reports)
        drawerView.headerAcceptedReports.text = it.completedReports.toString()
    }

    private fun generateWorkerObserver(drawerView: View): Observer<in Worker?> = Observer {
        it ?: return@Observer
        drawerView.tvHeaderFullName.text = it.displayName
        drawerView.headerTotalReports.text = it.totalCompletedReports.toString()
        drawerView.headerAcceptedReportsText.text =
            drawerView.context.getString(R.string.assigned_reports)
        drawerView.headerAcceptedReports.text = it.liveAssigned.toString()
    }

    private fun clearObservers() {
        if (userObserver != null) {
            activeUserViewModel.user.removeObserver(userObserver!!)
            userObserver = null
        }
        if (workerObserver != null) {
            activeUserViewModel.worker.removeObserver(workerObserver!!)
            workerObserver = null
        }
    }

}
