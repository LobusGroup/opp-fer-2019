package hr.fer.zemris.opp.ekoreport.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.RecyclerView
import com.firebase.ui.firestore.FirestoreRecyclerAdapter
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import hr.fer.zemris.opp.ekoreport.R
import hr.fer.zemris.opp.ekoreport.model.ActiveReportViewModel
import hr.fer.zemris.opp.ekoreport.model.Report
import hr.fer.zemris.opp.ekoreport.model.Worker
import hr.fer.zemris.opp.ekoreport.view.AvailableWorkersDialog
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.worker_entry.view.*

class AvailableWorkerAdapter(
    options: FirestoreRecyclerOptions<Worker>,
    private val dialog: AvailableWorkersDialog
) : FirestoreRecyclerAdapter<Worker, AvailableWorkerAdapter.ViewHolder>(options) {

    private val activeReportViewModel: ActiveReportViewModel by dialog.activityViewModels()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.worker_entry, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int, item: Worker) {
        holder.apply {
            tvName.text = item.displayName

            tvMiscData.text = item.miscData
            if (item.miscData.isNullOrBlank())
                tvMiscData.visibility = View.GONE
            else
                tvMiscData.visibility = View.VISIBLE

            @SuppressLint("SetTextI18n")
            tvCounters.text = "${item.liveAssigned}/${item.maxAssigned}"

            if (item.liveAssigned >= item.maxAssigned)
                containerView.visibility = View.GONE
            else
                containerView.visibility = View.VISIBLE

            singleWorkerRoot.setOnClickListener {
                activeReportViewModel.updateActiveReport(
                    mapOf(
                        "uidWorker" to item.documentId,
                        "statusCode" to Report.ASSIGNED
                    )
                )
                dialog.dismiss()
            }
        }
    }

    inner class ViewHolder(override val containerView: View) :
        RecyclerView.ViewHolder(containerView),
        LayoutContainer {

        val singleWorkerRoot: ViewGroup = containerView.singleWorkerRoot
        val tvName: TextView = containerView.tvName
        val tvMiscData: TextView = containerView.tvMiscData
        val tvCounters: TextView = containerView.tvCounters
    }

}
