package hr.fer.zemris.opp.ekoreport.fcm

import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.FirebaseMessagingService

class FCMReceiver : FirebaseMessagingService() {

    override fun onNewToken(token: String) = sendRegistrationToServer(token)

    companion object FCMUtil {

        private val TAG = FCMReceiver::class.java.simpleName

        private fun sendRegistrationToServer(token: String) {
            Firebase.firestore
                .collection("users")
                .document(FirebaseAuth.getInstance().uid.toString())
                .update("FCMtoken", token)
        }

        /** Sends device FCM token to the database and links it to the current user. */
        fun updateFCMToken() {
            //FCM - get current device notification token and send it to database
            FirebaseInstanceId.getInstance().instanceId
                .addOnCompleteListener {
                    if (!it.isSuccessful) {
                        Log.e(TAG, "getInstanceId failed", it.exception)
                        return@addOnCompleteListener
                    }

                    // Get new Instance ID token
                    val token = it.result!!.token
                    sendRegistrationToServer(token)
                }
        }
    }
}
