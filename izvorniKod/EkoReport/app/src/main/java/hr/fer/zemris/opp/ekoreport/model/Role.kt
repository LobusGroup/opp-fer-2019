package hr.fer.zemris.opp.ekoreport.model

enum class Role {
    REGULAR_USER,
    MODERATOR,
    WORKER
}
