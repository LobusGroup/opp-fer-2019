package hr.fer.zemris.opp.ekoreport.model

enum class NetworkState {
    NOT_MONITORED,
    AVAILABLE,
    UNAVAILABLE,
    LOST,
    LOSING
}
