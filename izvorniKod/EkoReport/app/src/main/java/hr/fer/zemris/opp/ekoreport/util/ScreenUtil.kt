package hr.fer.zemris.opp.ekoreport.util

import android.content.res.Resources
import android.util.TypedValue

internal fun Resources.fromDpToPx(dp: Float): Int =
    TypedValue.applyDimension(
        TypedValue.COMPLEX_UNIT_DIP,
        dp,
        displayMetrics
    ).toInt()
