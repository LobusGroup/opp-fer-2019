package hr.fer.zemris.opp.ekoreport.view

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.ktx.Firebase
import hr.fer.zemris.opp.ekoreport.R
import hr.fer.zemris.opp.ekoreport.adapter.UserReportsAdapter
import hr.fer.zemris.opp.ekoreport.model.ActiveReportViewModel
import hr.fer.zemris.opp.ekoreport.model.Report
import hr.fer.zemris.opp.ekoreport.model.Role
import hr.fer.zemris.opp.ekoreport.model.Role.*
import hr.fer.zemris.opp.ekoreport.util.showCategoriesDialog
import hr.fer.zemris.opp.ekoreport.util.update
import hr.fer.zemris.opp.ekoreport.view.HomePageFragment.FloatingButtonListener
import kotlinx.android.synthetic.main.fragment_home_page.*

class HomePageFragment : AuthenticationSensitiveFragment<FloatingButtonListener>() {

    private val activeReportViewModel: ActiveReportViewModel by activityViewModels()
    private val mFirestore by lazy { Firebase.firestore }

    private lateinit var adapter: UserReportsAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_home_page, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activeUserViewModel.filterRequested.observe(viewLifecycleOwner) {
            if (it == true)
                showCategoriesDialog(
                    R.string.categories_list_all,
                    R.string.categories_filter_title,
                    activeUserViewModel.selectedCategoryFilter,
                    activeUserViewModel.filterRequested,
                    activeUserViewModel
                )
        }
        activeUserViewModel.selectedCategoryFilter.observe(viewLifecycleOwner) {
            updateAll(it)
        }
    }

    override fun onInitialLoading() =
        navController.navigate(R.id.action_global_initialLoadingFragment)


    override fun onAuthenticated() {
        updateSavedUserMode()
        if (activeReportViewModel.hasShareImage())
            navController.navigate(R.id.action_homePageFragment_to_reportEditorFragment)
    }

    override fun onRoleChanged() = updateAll(null)

    /** Set visibility of "New Report" button based on current user role */
    private fun updateNewReportByRole(role: Role?) {
        if (role == REGULAR_USER) {
            listener?.getFAB()?.show()
            listener?.getFAB()?.setOnClickListener {
                activeReportViewModel.setActiveReport(null)
                navController.navigate(R.id.action_homePageFragment_to_reportEditorFragment)
            }
        } else {
            listener?.getFAB()?.hide()
        }
    }

    // If category is not null it's the id of the category to be shown
    private fun updateAll(categoryId: Int?) {
        val reportsRef = mFirestore.collection("reports")

        updateNewReportByRole(activeUserViewModel.currentRole.value)
        var query = when (activeUserViewModel.currentRole.value) {

            REGULAR_USER -> reportsRef
                .whereEqualTo("uidUser", mAuth.currentUser!!.uid)
                .orderBy("timeCreated", Query.Direction.DESCENDING)

            WORKER -> reportsRef
                .whereEqualTo("uidWorker", mAuth.currentUser!!.uid)
                .whereEqualTo("statusCode", Report.ASSIGNED)
                .orderBy("timeCreated", Query.Direction.ASCENDING)

            MODERATOR -> reportsRef
                .whereEqualTo("statusCode", Report.PENDING)
                .orderBy("timeCreated", Query.Direction.ASCENDING)

            else -> null

        } ?: return

        if (categoryId != null) {
            query = query.whereEqualTo("category", categoryId)
        }

        val options = FirestoreRecyclerOptions.Builder<Report>()
            .setQuery(query) { it.toObject<Report>()!! }
            .setLifecycleOwner(viewLifecycleOwner)
            .build()

        adapter = UserReportsAdapter(options, this, navController)
        rvAllReports.layoutManager = LinearLayoutManager(requireContext())
        rvAllReports.adapter = adapter
        adapter.startListening()
    }

    private fun updateSavedUserMode() {

        fun updateRoleFromPref(newRole: Role) {
            if (newRole in activeUserViewModel.availableRoles.value!!)
                activeUserViewModel.currentRole.update(newRole)
        }

        val sharedPref = requireActivity().getSharedPreferences(
            getString(R.string.app_name), Context.MODE_PRIVATE
        )
        val preferredMode = sharedPref.getString(
            getString(R.string.preference_preferred_mode),
            getString(R.string.preference_mode_no_prefs)
        )

        when (preferredMode) {
            getString(R.string.preference_mode_regular_user) -> updateRoleFromPref(REGULAR_USER)
            getString(R.string.preference_mode_worker) -> updateRoleFromPref(WORKER)
            getString(R.string.preference_mode_moderator) -> updateRoleFromPref(MODERATOR)
        }
    }

    /** Callback interface for operations on the main FloatingActionButton. */
    interface FloatingButtonListener : StatusMessageListener {

        /**
         * Returns reference to the main FloatingActionButton.
         *
         * @return FloatingActionButton
         */
        fun getFAB(): FloatingActionButton

    }

    private companion object {
        @Suppress("unused")
        private val TAG = HomePageFragment::class.java.simpleName
    }

}
