package hr.fer.zemris.opp.ekoreport.view

import android.content.Context
import androidx.fragment.app.Fragment

abstract class ListenerFragment<T> : Fragment() {

    protected var listener: T? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        @Suppress("UNCHECKED_CAST")
        listener = context as T
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

}
