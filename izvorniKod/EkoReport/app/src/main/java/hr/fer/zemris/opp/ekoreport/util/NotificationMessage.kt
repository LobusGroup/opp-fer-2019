package hr.fer.zemris.opp.ekoreport.util

import androidx.annotation.StringRes

class NotificationMessage(@StringRes val resId: Int, val formatArgs: List<Any>? = null)
