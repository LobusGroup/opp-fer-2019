package hr.fer.zemris.opp.ekoreport.model

import com.google.firebase.Timestamp
import com.google.firebase.firestore.DocumentId
import com.google.firebase.firestore.Exclude
import com.google.firebase.firestore.GeoPoint
import hr.fer.zemris.opp.ekoreport.R

data class Report(
    @DocumentId val documentId: String = "",
    val address: String = "",
    var category: Int = 0,
    var description: String? = null,
    var imageComplete: String? = null,
    var imageReport: String = "",
    val location: GeoPoint = GeoPoint(0.0, 0.0),
    var statusCode: Int = 0,
    val timeCreated: Timestamp = Timestamp.now(),
    var title: String = "",
    val uidUser: String = "",
    var uidWorker: String? = null
) {

    @get:Exclude
    val addressShort: String by lazy {
        var tmpAddr = address.trim()
        tmpAddr = when {
            tmpAddr.endsWith("Hrvatska") -> tmpAddr.dropLast("Hrvatska".length)
            tmpAddr.endsWith("Croatia") -> tmpAddr.dropLast("Croatia".length)
            else -> tmpAddr
        }
        return@lazy tmpAddr.trimEnd(' ', ',')
    }

    companion object {
        const val PENDING = 0
        const val REJECTED = 1
        const val ASSIGNED = 2
        const val COMPLETED = 3

        fun statusCodeToStringRes(statusCode: Int): Int = when (statusCode) {
            PENDING -> R.string.status_pending
            ASSIGNED -> R.string.status_assigned
            REJECTED -> R.string.status_rejected
            COMPLETED -> R.string.status_completed
            else -> error("Unknown status code")
        }
    }
}
