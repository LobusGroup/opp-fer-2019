package hr.fer.zemris.opp.ekoreport.view

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.net.Uri
import android.os.Bundle
import android.os.Looper
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.exifinterface.media.ExifInterface
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.observe
import com.google.android.gms.location.*
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.firebase.firestore.GeoPoint
import hr.fer.zemris.opp.ekoreport.R
import hr.fer.zemris.opp.ekoreport.model.ActiveReportViewModel
import hr.fer.zemris.opp.ekoreport.model.NetworkState
import hr.fer.zemris.opp.ekoreport.model.NetworkStateViewModel
import hr.fer.zemris.opp.ekoreport.model.NetworkStateViewModel.Companion.isOnline
import hr.fer.zemris.opp.ekoreport.model.Report
import hr.fer.zemris.opp.ekoreport.util.*
import hr.fer.zemris.opp.ekoreport.util.CustomTextWatcher.Companion.addCustomTextChangedListener
import kotlinx.android.synthetic.main.fragment_report_editor.*
import java.io.File

class ReportEditorFragment : AuthenticationSensitiveFragment<StatusMessageListener>() {

    private val activeReportViewModel: ActiveReportViewModel by activityViewModels()
    private val networkStateViewModel: NetworkStateViewModel by activityViewModels()

    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var locationManager: LocationManager

    private var tempPhotoPath: String? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_report_editor, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activeReportViewModel.leaveReportImmediately.observe(viewLifecycleOwner) {
            if (it == true) {
                activeReportViewModel.leaveReportImmediately.update(false)
                exitFragment()
            }
        }

        networkStateViewModel.startMonitoringNetwork(requireContext())
        networkStateViewModel.networkAvailable.observe(viewLifecycleOwner) {

            fun setButtons(visible: Boolean) {
                // Don't touch buttons if only editing report
                if (activeReportViewModel.report.value != null)
                    return

                btnSubmit.visibility = if (visible) VISIBLE else GONE

                if (activeReportViewModel.report.value != null) {
                    btnTakePhoto.visibility = GONE
                    btnSelectPhoto.visibility = GONE
                } else {
                    btnTakePhoto.visibility = if (visible) VISIBLE else GONE
                    btnSelectPhoto.visibility = if (visible) VISIBLE else GONE
                }
            }

            when (it) {
                NetworkState.AVAILABLE -> {
                    setButtons(true)
                    activeReportViewModel.report.value ?: checkPermissions()
                }
                NetworkState.LOST -> {
                    listener?.showSnackbar(R.string.lost_connection)
                    setButtons(false)
                }
                NetworkState.UNAVAILABLE -> {
                    if (activeReportViewModel.report.value == null)
                        listener?.showSnackbar(R.string.no_network)
                    setButtons(false)
                }
                else -> Unit
            }
        }

        locationManager = requireActivity()
            .getSystemService(Context.LOCATION_SERVICE) as LocationManager

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())

        // Confirmation dialog if changes were made
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {

            // Disable back button while submitting report
            if (activeReportViewModel.currentlySubmittingReport.value == true) {
                listener?.showSnackbar(R.string.report_upload_in_progress)
                return@addCallback
            }

            if (changesMade()) showConfirmationDialog()
            else exitFragment()
        }

        activeReportViewModel.categoryChooserOpen.observe(viewLifecycleOwner) {
            if (it == true)
                showCategoriesDialog(
                    R.string.categories_chooser_not_chosen,
                    R.string.categories_chooser_title,
                    activeReportViewModel.selectedCategoryId,
                    activeReportViewModel.categoryChooserOpen,
                    activeUserViewModel
                )
        }
        activeReportViewModel.selectedCategoryId.observe(viewLifecycleOwner) { id ->
            tvCategoryLocalSelected.text = if (id == null)
                getString(R.string.categories_chooser_not_chosen)
            else
                activeUserViewModel.categoriesMap!!.getValue(id).titleLocal
        }

        val reportObserver = Observer<Report?> {

            if (it == null) {
                tvReportEditorTitle.text = getString(R.string.new_report)
                tilReportPhoto.visibility = GONE
                if (activeReportViewModel.hasPhoto()) {
                    activeReportViewModel.loadImage(tilReportPhoto)
                    btnSubmit.isEnabled = true
                }
                return@Observer
            }

            tvReportEditorTitle.text = getString(R.string.edit_report)
            etEditTitle.setText(it.title)
            etEditDescription.setText(it.description)

            // Only allow editing of non-assigned reports
            val isPending = it.statusCode == Report.PENDING
            btnSubmit.isEnabled = isPending
            etEditTitle.isEnabled = isPending
            etEditDescription.isEnabled = isPending
            btnCategorySelect.visibility = if (isPending) VISIBLE else GONE

            // Do not allow taking photos if only editing report
            btnTakePhoto.isEnabled = false
            btnSelectPhoto.isEnabled = false
            btnTakePhoto.visibility = GONE
            btnSelectPhoto.visibility = GONE

            tvAddress.text = it.address
            btnSubmit.text = getString(
                if (!isPending) R.string.warning_report_assigned
                else R.string.report_submit
            )

            activeReportViewModel.downloadImageInto(tilReportPhoto)
        }

        activeReportViewModel.report.observe(viewLifecycleOwner, reportObserver)

        activeReportViewModel.address.observe(viewLifecycleOwner) {
            if (it != null) tvAddress.text = it
        }

        // Set button listeners
        btnSubmit.setOnClickListener { submitChanges() }
        btnTakePhoto.setOnClickListener {
            launchCamera("REPORT", REQUEST_TAKE_PHOTO) { tempPhotoPath = it }
        }
        btnSelectPhoto.setOnClickListener { launchGallery() }
        btnCategorySelect.setOnClickListener {
            activeReportViewModel.categoryChooserOpen.value = true
        }

        addCustomTextChangedListener(
            etEditTitle,
            tilEditTitle,
            CheckInputConstraints::checkMandatoryTextConstraints
        )

        checkShare()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        networkStateViewModel.stopMonitoringNetwork()
    }

    override fun onRoleChanged() {
        if (activeReportViewModel.report.value != null
            && !reportAccessAllowed(
                activeReportViewModel.report.value!!,
                activeUserViewModel.currentRole.value!!,
                mAuth.currentUser!!.uid,
                true
            )
        ) exitFragment()
    }

    private fun checkShare() {
        if (!activeReportViewModel.hasShareImage())
            return

        val uri = activeReportViewModel.getSharedImageUri()
        activeReportViewModel.finishShare()

        if (!isOnline(requireContext()))
            listener?.showSnackbar(R.string.share_image_from_gallery_offline)

        processImageFromGallery(uri)
    }

    private fun processImageFromGallery(uri: Uri) {
        // Check if image is valid and has valid EXIF GPS data
        val tempLocation: GeoPoint? = getExifLocation(uri)
        if (tempLocation == null) {
            listener?.showSnackbar(R.string.image_from_gallery_missing_location)
            return
        }

        activeReportViewModel.setLocation(tempLocation, requireContext())
        activeReportViewModel.loadImage(tilReportPhoto, uri)
        btnSubmit.isEnabled = true
    }

    /** Launches a gallery image select intent. */
    private fun launchGallery() {
        val pickIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        pickIntent.type = "image/*"
        startActivityForResult(pickIntent, REQUEST_SELECT_PHOTO)
    }

    // When photo is taken, resize it to fit the view, rotate it and then display it
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            REQUEST_TAKE_PHOTO ->
                if (resultCode == Activity.RESULT_OK) {
                    requestLocation()
                    activeReportViewModel.loadImage(
                        tilReportPhoto,
                        Uri.fromFile(File(tempPhotoPath!!))
                    )
                    // Enable the submit button
                    btnSubmit.isEnabled = true
                }

            REQUEST_SELECT_PHOTO ->
                if (resultCode == Activity.RESULT_OK)
                    processImageFromGallery(data!!.data!!)
        }
    }

    private fun getCoordDMS(exifString: String, exifRef: String?): Double {
        val splitString = exifString.split(",")
        val firstPart = splitString[0].split("/")
        val secondPart = splitString[1].split("/")
        val thirdPart = splitString[2].split("/")

        val first = firstPart[0].toDouble() / firstPart[1].toDouble()
        val second = secondPart[0].toDouble() / (secondPart[1].toDouble() * 60)
        val third = thirdPart[0].toDouble() / (thirdPart[1].toDouble() * 3600)

        var total = first + second + third
        if (exifRef != "N" && exifRef != "E")
            total = -total
        return total
    }

    private fun getExifLocation(uri: Uri): GeoPoint? {

        val exifInterface = ExifInterface(requireActivity().contentResolver.openInputStream(uri)!!)
        val lat = exifInterface.getAttribute(ExifInterface.TAG_GPS_LATITUDE)
        val lon = exifInterface.getAttribute(ExifInterface.TAG_GPS_LONGITUDE)
        val latRef = exifInterface.getAttribute(ExifInterface.TAG_GPS_LATITUDE_REF)
        val lonRef = exifInterface.getAttribute(ExifInterface.TAG_GPS_LONGITUDE_REF)

        return if (lat == null || lon == null)
            null
        else
            GeoPoint(getCoordDMS(lat, latRef), getCoordDMS(lon, lonRef))
    }

    private fun submitChanges() {

        // Load data from text fields
        val newTitle = etEditTitle.text.toString().trim()
        val newDescription = etEditDescription.text.toString().trim()
        val newCategoryID = activeReportViewModel.selectedCategoryId.value
        val uid = mAuth.currentUser!!.uid

        // Check text fields
        with(ErrorCollator(resources)) {
            check(newTitle, tilEditTitle, CheckInputConstraints::checkMandatoryTextConstraints)
            if (errorDetected)
                return
        }

        if (newCategoryID == null) {
            listener?.showSnackbar(R.string.categories_chooser_not_chosen_snackbar)
            return
        }

        activeUserViewModel.currentlyLoading.update(true)
        if (activeReportViewModel.report.value == null
            && !activeReportViewModel.receivedLocation.value!!
        ) {
            activeReportViewModel.receivedLocation.observe(viewLifecycleOwner) {
                if (it == true) {
                    submitChangesChecked(uid, newTitle, newDescription, newCategoryID)
                }
            }
        } else {
            submitChangesChecked(uid, newTitle, newDescription, newCategoryID)
        }
    }

    private fun submitChangesChecked(
        uid: String,
        title: String,
        description: String,
        category: Int
    ) {
        activeReportViewModel.submitChanges(uid, title, description, category)
        activeReportViewModel.currentlySubmittingReport.observe(viewLifecycleOwner) {
            if (it == false) {
                activeUserViewModel.currentlyLoading.update(false)
                exitFragment()
            }
        }
    }

    // Determine if anything changed (or we are creating a new report) for showing the confirmation dialog
    private fun changesMade(): Boolean = with(activeReportViewModel.report.value) {
        if (this == null) {
            etEditTitle.text.toString().trim().isNotEmpty()
                    || etEditDescription.text.toString().trim().isNotEmpty()
                    || activeReportViewModel.selectedCategoryId.value != null
                    || activeReportViewModel.hasPhoto()
        } else {
            val desc = if (description == null) "" else description
            title != etEditTitle.text.toString().trim()
                    || desc != etEditDescription.text.toString().trim()
                    || this.category != activeReportViewModel.selectedCategoryId.value
        }
    }

    /** Checks for permission nad requests if necessary */
    private fun checkPermissions(): Boolean {
        if (!hasPermissions()) {
            btnTakePhoto.isEnabled = false
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    requireActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION
                )
            ) {
                MaterialAlertDialogBuilder(requireContext())
                    .setTitle(R.string.request_location_permission)
                    .setPositiveButton(android.R.string.ok) { _, _ ->
                        requestLocationPermission()
                    }.setOnDismissListener {
                        requestLocationPermission()
                    }.show()
            } else {
                requestLocationPermission()
            }
            return false
        } else {
            if (!isLocationEnabled()) {
                btnTakePhoto.isEnabled = false
                listener?.showSnackbar(R.string.request_gps_enabled)
            }
            return isLocationEnabled()
        }
    }

    private fun requestLocation() {
        if (checkPermissions())
            requestNewLocationData()
    }

    private fun hasPermissions(): Boolean =
        ContextCompat.checkSelfPermission(
            requireActivity(),
            Manifest.permission.ACCESS_COARSE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
            requireActivity(),
            Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED

    private fun requestLocationPermission() {
        requestPermissions(
            arrayOf(
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
            ),
            PERMISSION_ID
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        if (requestCode == PERMISSION_ID) {
            if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                btnTakePhoto.isEnabled = true
            } else {
                listener?.showSnackbar(R.string.location_permission_not_granted)
                btnTakePhoto.isEnabled = false
            }
        }
    }

    /** Checks if GPS location is enabled, network location is not accurate enough. */
    private fun isLocationEnabled(): Boolean =
        (locationManager).isProviderEnabled(
            LocationManager.GPS_PROVIDER
        )

    private fun requestNewLocationData() {
        val mLocationRequest = LocationRequest()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.interval = 0
        mLocationRequest.fastestInterval = 0
        mLocationRequest.numUpdates = 1

        fusedLocationClient.requestLocationUpdates(
            mLocationRequest, mLocationCallback,
            Looper.myLooper()
        )
    }

    private val mLocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            val mLastLocation: Location = locationResult.lastLocation
            val locationGeoPoint = GeoPoint(mLastLocation.latitude, mLastLocation.longitude)
            activeReportViewModel.setLocation(
                locationGeoPoint, requireContext()
            )
        }
    }

    private companion object {
        private const val REQUEST_TAKE_PHOTO = 12325    // Intent action identifier
        private const val REQUEST_SELECT_PHOTO = 12326  // Intent action identifier
        private const val PERMISSION_ID = 42
    }

}
