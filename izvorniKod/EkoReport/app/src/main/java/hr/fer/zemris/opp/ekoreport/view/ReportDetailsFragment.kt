package hr.fer.zemris.opp.ekoreport.view

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.util.Linkify
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.observe
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import hr.fer.zemris.opp.ekoreport.R
import hr.fer.zemris.opp.ekoreport.model.*
import hr.fer.zemris.opp.ekoreport.model.Report.Companion.statusCodeToStringRes
import hr.fer.zemris.opp.ekoreport.util.*
import kotlinx.android.synthetic.main.fragment_report_details.*
import java.io.File

class ReportDetailsFragment : AuthenticationSensitiveFragment<StatusMessageListener>() {

    private val mFirestore by lazy { Firebase.firestore }
    private val mStorage by lazy { Firebase.storage }
    private val activeReportViewModel: ActiveReportViewModel by activityViewModels()
    private val map by lazy { MutableLiveData<GoogleMap?>(null) }

    private lateinit var currentPhotoPath: String   // Local file path of the report completed photo

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_report_details, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Initialize map
        (childFragmentManager.findFragmentById(R.id.map_fragment) as SupportMapFragment)
            .getMapAsync { map.update(it) }

        fixMapScrolling()

        val reportObserver = Observer<Report?> { report ->
            if (report == null) {
                exitFragment()
                return@Observer
            }

            map.observe(viewLifecycleOwner) {
                it ?: return@observe

                with(activeReportViewModel.report.value!!) {
                    val latlng = LatLng(location.latitude, location.longitude)

                    map.value!!.clear()

                    map.value!!.addMarker(
                        MarkerOptions().position(latlng)
                            .title(title)
                            .snippet(description)
                    )

                    map.value!!.moveCamera(CameraUpdateFactory.newLatLngZoom(latlng, 15F))
                }
            }

            tvTitle.text = report.title

            with(report.description) {
                tvDescription.text = this
                descriptionLayout.visibility = if (isNullOrBlank()) GONE else VISIBLE
            }

            tvAddress.text = report.address
            tvCategory.text =
                activeUserViewModel.categoriesMap!!.getValue(report.category).titleLocal
            tvStatus.text = getString(statusCodeToStringRes(report.statusCode))
            tvDatePosted.text = report.timeCreated.getDateString()
            tvTime.text = report.timeCreated.getTimeString()

            // Download and display report image with placeholder loader
            activeReportViewModel.downloadImageInto(ivReport, report.imageReport, true)

            if (report.imageComplete == null) {
                ivArrowToResolvedImage.visibility = GONE
                ivCompleted.visibility = GONE
            } else {
                ivArrowToResolvedImage.visibility = VISIBLE
                activeReportViewModel.downloadImageInto(ivCompleted, report.imageComplete!!, true)
            }

            reloadView(
                activeUserViewModel.currentRole.value!!,
                activeReportViewModel.report.value!!.uidWorker,
                activeReportViewModel.report.value!!.statusCode
            )
        }

        activeReportViewModel.report.observe(viewLifecycleOwner, reportObserver)
    }

    private fun fixMapScrolling() = transparentImageOverMap.setOnTouchListener { _, event ->
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                scrollViewParent.requestDisallowInterceptTouchEvent(true)
                false
            }

            MotionEvent.ACTION_UP -> {
                scrollViewParent.requestDisallowInterceptTouchEvent(false)
                true
            }

            MotionEvent.ACTION_MOVE -> {
                scrollViewParent.requestDisallowInterceptTouchEvent(true)
                false
            }

            else -> true
        }
    }

    override fun onRoleChanged() =
        reloadView(
            activeUserViewModel.currentRole.value!!,
            activeReportViewModel.report.value!!.uidWorker,
            activeReportViewModel.report.value!!.statusCode
        )

    private fun reloadView(currentRole: Role, workerId: String?, statusCode: Int) {
        if (!reportAccessAllowed(
                activeReportViewModel.report.value!!,
                activeUserViewModel.currentRole.value!!,
                mAuth.currentUser!!.uid,
                true
            )
        ) {
            exitFragment()
            return
        }

        when (currentRole) {

            Role.REGULAR_USER -> {
                if (statusCode == Report.PENDING) {
                    btnEditReport.visibility = VISIBLE
                    btnEditReport.setOnClickListener {
                        activeReportViewModel.selectedCategoryId.update(
                            activeReportViewModel.report.value!!.category
                        )
                        navController.navigate(R.id.action_reportDetailsFragment_to_reportEditorFragment)
                    }
                    btnDeleteReport.visibility = VISIBLE
                    btnDeleteReport.setOnClickListener { deleteReport() }
                } else {
                    btnEditReport.visibility = GONE
                    btnDeleteReport.visibility = GONE
                }

                btnCompleteReport.visibility = GONE
                btnRejectReport.visibility = GONE
                btnUnassign.visibility = GONE
                layoutUserDetails.visibility = GONE
                btnAssign.visibility = GONE
                btnBanUser.visibility = GONE
                assignedToLayout.visibility = GONE
            }

            Role.WORKER -> {
                workerAndModeratorOptions()

                if (statusCode == Report.ASSIGNED) {
                    btnCompleteReport.visibility = VISIBLE
                    btnCompleteReport.setOnClickListener { takeReportCompletedPhoto() }
                    btnRejectReport.visibility = VISIBLE
                    btnRejectReport.setOnClickListener { rejectReport(Report.PENDING) }
                } else {
                    btnCompleteReport.visibility = GONE
                    btnRejectReport.visibility = GONE
                }

                btnDeleteReport.visibility = GONE
                btnUnassign.visibility = GONE
                layoutUserDetails.visibility = GONE
                btnEditReport.visibility = GONE
                btnAssign.visibility = GONE
                btnBanUser.visibility = GONE
                assignedToLayout.visibility = GONE
            }

            Role.MODERATOR -> {
                btnDeleteReport.visibility = GONE
                btnCompleteReport.visibility = GONE
                assignedToLayout.visibility = GONE

                when (statusCode) {
                    Report.PENDING -> {
                        workerAndModeratorOptions()

                        btnRejectReport.visibility = VISIBLE
                        btnRejectReport.setOnClickListener { rejectReport(Report.REJECTED) }
                        btnUnassign.visibility = GONE
                        btnEditReport.visibility = VISIBLE
                        btnEditReport.setOnClickListener {
                            activeReportViewModel.selectedCategoryId.update(
                                activeReportViewModel.report.value!!.category
                            )
                            navController.navigate(R.id.action_reportDetailsFragment_to_reportEditorFragment)
                        }
                        btnAssign.visibility = VISIBLE
                        btnAssign.setOnClickListener { showAvailableWorkers() }
                        btnBanUser.visibility = VISIBLE
                        btnBanUser.setOnClickListener { banUser() }
                        btnAssign.text = getString(R.string.assign)
                    }

                    Report.ASSIGNED -> {
                        assignedToLayout.visibility = VISIBLE
                        mFirestore.collection("workers")
                            .document(workerId!!)
                            .get()
                            .addOnSuccessListener {
                                if (tvAssignedTo != null)
                                    tvAssignedTo.text = it.toObject<Worker>()!!.displayName
                            }
                        btnAssign.text = getString(R.string.reassign)
                        btnAssign.visibility = VISIBLE
                        btnAssign.setOnClickListener { showAvailableWorkers() }
                        btnEditReport.visibility = GONE
                        btnBanUser.visibility = GONE
                        btnUnassign.visibility = VISIBLE
                        btnUnassign.setOnClickListener { unassignReport() }
                    }

                    else -> {
                        btnUnassign.visibility = GONE
                        btnBanUser.visibility = GONE
                        btnAssign.visibility = GONE
                        btnEditReport.visibility = GONE
                    }
                }
            }
        }
    }

    private fun deleteReport() {
        MaterialAlertDialogBuilder(requireContext())
            .setTitle(R.string.delete_report_dialog)
            .setPositiveButton(android.R.string.yes) { _, _ ->
                activeReportViewModel.deleteCurrentReport()
                exitFragment()
            }.setNegativeButton(android.R.string.no, null)
            .show()
    }

    private fun unassignReport() {
        activeReportViewModel.updateActiveReport(
            mapOf(
                "statusCode" to Report.PENDING,
                "uidWorker" to null
            )
        )
    }

    private fun rejectReport(newStatusCode: Int) {
        MaterialAlertDialogBuilder(requireContext())
            .setTitle(R.string.reject_dialog)
            .setPositiveButton(android.R.string.yes) { _, _ ->
                activeReportViewModel.updateActiveReport(
                    mapOf(
                        "statusCode" to newStatusCode,
                        "uidWorker" to null
                    )
                )
                listener?.showSnackbar(R.string.report_rejected_message)
                exitFragment()
            }
            .setNegativeButton(android.R.string.no, null)
            .show()
    }

    private fun workerAndModeratorOptions() {
        mFirestore.collection("users")
            .document(activeReportViewModel.report.value!!.uidUser)
            .get()
            .addOnSuccessListener {
                val reportUser = it.toObject<User>()!!

                layoutUserDetails.visibility = VISIBLE
                ivStarCredible.visibility = if (reportUser.credible) VISIBLE else GONE
                tvUserRating.text = getUserRatingString(reportUser)
                tvUserName.text = reportUser.displayName
                tvUserPhone.text = reportUser.phone
                Linkify.addLinks(tvUserPhone, Linkify.PHONE_NUMBERS)
                tvUserEmail.text = reportUser.email
                Linkify.addLinks(tvUserEmail, Linkify.EMAIL_ADDRESSES)
            }
    }

    @SuppressLint("StringFormatInvalid")
    private fun getUserRatingString(reportUser: User): String = with(reportUser) {
        val numberFinished = completedReports + rejectedReports
        return if (numberFinished <= 0) getString(R.string.no_rating)
        else getString(
            R.string.rating_text,
            100.0 * completedReports / numberFinished,
            totalReports
        )
    }

    private fun showAvailableWorkers() =
        AvailableWorkersDialog().show(fragmentManager!!, "workerDialog")

    private fun takeReportCompletedPhoto() = launchCamera("COMPLETED", REQUEST_TAKE_PHOTO) {
        currentPhotoPath = it
    }

    private fun banUser() {
        MaterialAlertDialogBuilder(requireContext())
            .setTitle(R.string.ban_dialog_title)
            .setMessage(R.string.ban_dialog_message)
            .setPositiveButton(android.R.string.yes) { _, _ ->
                mFirestore.collection("users")
                    .document(activeReportViewModel.report.value!!.uidUser)
                    .update("banned", true)
            }.setNegativeButton(android.R.string.no, null)
            .show()
    }

    // When photo is taken, resize it to fit the view, rotate it and then display it
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == Activity.RESULT_OK) {
            uploadPhotoAndSendData()
        }
    }

    private fun uploadPhotoAndSendData() {
        val storageRef = mStorage.reference
        val reportPhotoFile = Uri.fromFile(File(currentPhotoPath))
        val imageCompletedReport =
            "reportImages/${activeReportViewModel.report.value!!.uidUser}/${reportPhotoFile.lastPathSegment}"
        val reportImagesRef = storageRef.child(imageCompletedReport)
        reportImagesRef.putFile(reportPhotoFile)
        activeReportViewModel.updateActiveReport(
            mapOf(
                "imageComplete" to imageCompletedReport,
                "statusCode" to Report.COMPLETED
            )
        )
        listener?.showSnackbar(R.string.report_completed_message)
        exitFragment()
    }

    private companion object {
        private const val REQUEST_TAKE_PHOTO = 12340   // Intent action identifier
    }

}
