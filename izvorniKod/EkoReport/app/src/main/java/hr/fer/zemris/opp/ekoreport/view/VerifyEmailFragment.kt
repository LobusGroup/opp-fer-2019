package hr.fer.zemris.opp.ekoreport.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import hr.fer.zemris.opp.ekoreport.R
import kotlinx.android.synthetic.main.fragment_verify_email.*

class VerifyEmailFragment : AuthenticationSensitiveFragment<StatusMessageListener>() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_verify_email, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        tvVerifyEmailShowingAddress.text = activeUserViewModel.user.value!!.email

        btnRefreshStatus.setOnClickListener {
            activeUserViewModel.emailVerificationStatusRefresh()
        }

        btnResendVerificationEmail.setOnClickListener {
            activeUserViewModel.emailVerificationResend()
        }

        btnSignOut.setOnClickListener {
            activeUserViewModel.signOut()
        }

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            requireActivity().finish()
        }
    }

    override fun onAuthenticated() {
        showRegisterWelcomeMessage()
        exitFragment()
    }

    override fun onEmailNotVerified() = Unit

}
