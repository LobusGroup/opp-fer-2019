package hr.fer.zemris.opp.ekoreport.util

import hr.fer.zemris.opp.ekoreport.model.Report
import hr.fer.zemris.opp.ekoreport.model.Role
import hr.fer.zemris.opp.ekoreport.model.Role.*

internal fun reportAccessAllowed(
    report: Report,
    currentRole: Role,
    currentUserId: String,
    forceAllowModerator: Boolean = false
): Boolean = when (currentRole) {
    REGULAR_USER -> report.uidUser == currentUserId
    WORKER -> report.uidWorker == currentUserId
    MODERATOR -> forceAllowModerator || report.statusCode == Report.PENDING
}
