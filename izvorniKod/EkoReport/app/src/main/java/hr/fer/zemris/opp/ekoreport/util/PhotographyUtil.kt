package hr.fer.zemris.opp.ekoreport.util

import android.content.Intent
import android.net.Uri
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.ImageView
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.google.firebase.storage.StorageReference
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

/** Launches Camera by Intent. */
internal fun Fragment.launchCamera(
    prefix: String,
    requestCode: Int,
    block: (String) -> Unit
) {
    val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
    takePictureIntent.resolveActivity(requireActivity().packageManager) ?: return

    val photoFile = try {
        createImageFile(prefix).apply { block(absolutePath) }
    } catch (e: IOException) {
        Log.e("PhotographyUtil", "Error while creating photo file", e)
        return
    }

    // NOTE: authority must be defined in the manifest
    val photoURI: Uri = FileProvider.getUriForFile(
        requireActivity(),
        "hr.fer.zemris.opp.ekoreport.fileprovider",
        photoFile
    )
    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
    // Take the actual photo instead of bitmap preview!
    startActivityForResult(takePictureIntent, requestCode)
}

/** Creates an empty file for storing the image. */
@Throws(IOException::class)
internal fun Fragment.createImageFile(prefix: String): File {
    // Create an image file name
    val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss", Locale("en"))
        .format(Date())
    val storageDir: File = requireActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES)!!
    return File.createTempFile(
        "${prefix}_${timeStamp}_",
        ".jpg",
        storageDir
    )
}

internal fun loadImageLocal(uri: Uri?, iv: ImageView) {
    val requestOptions = prepareForImageLoading(iv)
    Glide.with(iv.context)
        .load(uri)
        .apply(requestOptions)
        .into(iv)
        .also { iv.visibility = View.VISIBLE }
}

internal fun loadImageRemote(ref: StorageReference, iv: ImageView, circular: Boolean = false) {
    val requestOptions = prepareForImageLoading(iv)
    val glideRequest = GlideApp.with(iv.context)
        .load(ref)
        .apply(requestOptions)

    if (circular)
        glideRequest.circleCrop()

    glideRequest.into(iv)
}

private fun prepareForImageLoading(iv: ImageView): RequestOptions {
    val circularProgressDrawable = CircularProgressDrawable(iv.context)
    circularProgressDrawable.strokeWidth = 5f
    circularProgressDrawable.centerRadius = 30f
    circularProgressDrawable.start()

    return RequestOptions()
        .diskCacheStrategy(DiskCacheStrategy.ALL)
        .placeholder(circularProgressDrawable)
}
