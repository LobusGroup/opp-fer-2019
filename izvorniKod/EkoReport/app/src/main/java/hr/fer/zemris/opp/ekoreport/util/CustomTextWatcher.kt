package hr.fer.zemris.opp.ekoreport.util

import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import com.google.android.material.textfield.TextInputLayout

class CustomTextWatcher(
    private val editText: EditText,
    private val textInputLayout: TextInputLayout,
    private val evaluationFunction: (String) -> CustomInputErrors
) : TextWatcher {

    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

    override fun afterTextChanged(s: Editable) {}

    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        val editTextContent = editText.text.toString()

        if (editTextContent.isEmpty()) {
            textInputLayout.error = null
            return
        }

        val contentReport = evaluationFunction(editTextContent)
        if (!contentReport.valid)
            textInputLayout.error = textInputLayout.context.getString(contentReport.message)
        else
            textInputLayout.error = null
    }

    companion object {

        fun addCustomTextChangedListener(
            et: EditText,
            til: TextInputLayout,
            f: (String) -> CustomInputErrors
        ) {
            et.addTextChangedListener(CustomTextWatcher(et, til, f))
        }

    }

}
