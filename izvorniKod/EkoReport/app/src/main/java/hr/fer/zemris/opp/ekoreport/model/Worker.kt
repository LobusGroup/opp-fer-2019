package hr.fer.zemris.opp.ekoreport.model

import com.google.firebase.firestore.DocumentId
import com.google.firebase.firestore.Exclude

data class Worker(
    @DocumentId val documentId: String = "",
    val categories: ArrayList<Int> = arrayListOf(),
    val firstName: String = "",
    val lastName: String = "",
    val miscData: String? = null,
    val liveAssigned: Int = 0,
    val maxAssigned: Int = 0,
    val totalCompletedReports: Int = 0
) {
    @get:Exclude
    val displayName: String by lazy { "$firstName $lastName" }
}
