package hr.fer.zemris.opp.ekoreport.model

import android.util.Log
import androidx.lifecycle.*
import com.google.firebase.FirebaseException
import com.google.firebase.auth.EmailAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthException
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.EventListener
import com.google.firebase.firestore.ListenerRegistration
import com.google.firebase.firestore.QuerySnapshot
import com.google.firebase.firestore.Source
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.firestore.ktx.toObjects
import com.google.firebase.ktx.Firebase
import hr.fer.zemris.opp.ekoreport.R
import hr.fer.zemris.opp.ekoreport.fcm.FCMReceiver
import hr.fer.zemris.opp.ekoreport.model.ActiveUserViewModel.AuthenticationState.*
import hr.fer.zemris.opp.ekoreport.model.Role.*
import hr.fer.zemris.opp.ekoreport.util.*
import kotlinx.coroutines.*
import kotlinx.coroutines.tasks.asDeferred
import kotlinx.coroutines.tasks.await
import java.text.Collator
import java.util.*
import kotlin.Comparator

/** Singleton ViewModel holding information about the currently logged-in user. */
class ActiveUserViewModel : ViewModel() {

    /** Enumeration for all possible authentication states. */
    enum class AuthenticationState {

        /** Authentication state unknown, loading data during application startup. */
        INITIAL_LOADING,

        /** No user is logged in, no errors to report. */
        UNAUTHENTICATED,

        /** User is logged in, not banned, with email verified. */
        AUTHENTICATED,

        /** User that logged-in is banned. */
        BANNED,

        /** User successfully logged in, but email is not verified. */
        EMAIL_NOT_VERIFIED

    }

    private val mAuth by lazy { FirebaseAuth.getInstance() }
    private val mFirestore by lazy { Firebase.firestore }

    /** Current authentication state. */
    val authenticationState by lazy { MutableLiveData<AuthenticationState>(INITIAL_LOADING) }

    /** Currently logged-in user. */
    val user by lazy { MutableLiveData<User?>() }

    /** Currently logged-in worker. */
    val worker by lazy { MutableLiveData<Worker?>() }

    /** Currently used role. */
    val currentRole by lazy { MutableLiveData<Role?>() }

    /** Possible roles for current user. */
    val availableRoles by lazy { MutableLiveData<MutableSet<Role>>(mutableSetOf()) }

    /** Defines whether or not the loading indicator should be shown. */
    val currentlyLoading by lazy { MutableLiveData(false) }

    /** Defines whether or not the navigation drawer should be forcefully closed. */
    val forceDrawerLock by lazy { MutableLiveData(true) }

    /** View Model's notification stream. */
    val notificationChannel by lazy { MutableLiveData<NotificationMessage?>(null) }

    /** List of all categories sorted by [Category.categoryId] in ascending order. */
    var categoriesList: List<Category>? = null

    /** Map of all categories - [Category.categoryId] to [Category]. */
    var categoriesMap: Map<Int, Category>? = null

    /** Should Filter icon be visible. */
    val filterIconVisible by lazy { MutableLiveData<Boolean>(false) }

    /** Should category filter dialog be shown. */
    val filterRequested by lazy { MutableLiveData(false) }

    /** Active category filter ([Category.categoryId]), `null` if no filter is applied. */
    val selectedCategoryFilter by lazy { MutableLiveData<Int?>(null) }


    private var usersListener: ListenerRegistration? = null
    private var workersListener: ListenerRegistration? = null
    private var moderatorsListener: ListenerRegistration? = null

    private val userEventListener = EventListener<DocumentSnapshot> { snapshot, e ->
        if (e != null) {
            Log.e(TAG, "userEventListener:error", e)
            return@EventListener
        }

        if (snapshot?.exists() == true) {
            user.update(snapshot.toObject<User>()!!)
            availableRoles.customAdd(REGULAR_USER)
            if (user.value!!.banned) {
                authenticationState.update(BANNED)
                detachAllSnapshotListeners()
            }
        } else {
            // This is either a critical database error
            // or Administrator tried to log in without having User powers.
            Log.e(TAG, "Missing core user data!")
        }
    }

    private val workerEventListener = EventListener<DocumentSnapshot> { snapshot, e ->
        if (e != null) {
            // This will most likely be triggered when user gets banned.
            // Listener will automatically be detached.
            Log.e(TAG, "workerEventListener:error", e)
            return@EventListener
        }

        if (snapshot?.exists() == true) {
            worker.update(snapshot.toObject<Worker>()!!)
            availableRoles.customAdd(WORKER)
        } else {
            availableRoles.customRemove(WORKER)
            if (currentRole.value == WORKER)
                currentRole.update(REGULAR_USER)
            worker.update(null)
        }
    }

    private val moderatorEventListener = EventListener<DocumentSnapshot> { snapshot, e ->
        if (e != null) {
            // This will most likely be triggered when user gets banned.
            // Listener will automatically be detached.
            Log.e(TAG, "moderatorEventListener:error", e)
            return@EventListener
        }

        if (snapshot?.exists() == true) {
            availableRoles.customAdd(MODERATOR)
        } else {
            availableRoles.customRemove(MODERATOR)
            if (currentRole.value == MODERATOR)
                currentRole.update(REGULAR_USER)
        }
    }

    init {
        viewModelScope.launch(Dispatchers.Main) { initialization() }
    }

    private suspend fun initialization() = withContext(Dispatchers.Main) {
        if (mAuth.currentUser == null)
            resetDataUnsafe(UNAUTHENTICATED)
        else
            startup()
    }

    private fun attachAllSnapshotListeners() {
        usersListener = attachListener(
            "users",
            mAuth.currentUser!!.uid,
            userEventListener
        )
        workersListener = attachListener(
            "workers",
            mAuth.currentUser!!.uid,
            workerEventListener
        )
        moderatorsListener = attachListener(
            "moderators",
            mAuth.currentUser!!.uid,
            moderatorEventListener
        )
    }

    private fun detachAllSnapshotListeners() {
        detachListener(::usersListener)
        detachListener(::workersListener)
        detachListener(::moderatorsListener)
    }

    fun authenticate(email: String, password: String) =
        viewModelScope.launch(Dispatchers.Main) {
            currentlyLoading.value = true
            authenticateInternal(email, password)
            currentlyLoading.value = false
        }

    private suspend fun authenticateInternal(email: String, password: String) =
        withContext(Dispatchers.Main) {
            try {
                withTimeout(3000L) {
                    mAuth.signInWithEmailAndPassword(email, password).await()
                }
            } catch (e: Exception) {
                when (e) {
                    is FirebaseAuthException -> {
                        Log.e(TAG, "Authentication error", e)
                        resetDataUnsafe(
                            UNAUTHENTICATED,
                            NotificationMessage(R.string.unsuccessful_login_message)
                        )
                        return@withContext
                    }
                    is FirebaseException, is TimeoutCancellationException -> {
                        Log.e(TAG, "Authentication network error", e)
                        resetDataUnsafe(
                            UNAUTHENTICATED,
                            NotificationMessage(R.string.network_issue_unspecific)
                        )
                        return@withContext
                    }
                    else -> throw e
                }
            }

            startup()
        }

    fun register(newUser: User, password: String) =
        viewModelScope.launch(Dispatchers.Main) {
            currentlyLoading.value = true
            registerInternal(newUser, password)
            currentlyLoading.value = false
        }

    private suspend fun registerInternal(newUser: User, password: String) =
        withContext(Dispatchers.Main) {
            var shouldDelete = false
            try {
                withTimeout(6000L) {
                    mAuth.createUserWithEmailAndPassword(newUser.email, password)
                        .addOnCompleteListener {
                            if (shouldDelete) mAuth.currentUser?.delete()
                        }.await()
                    mFirestore.collection("users")
                        .document(mAuth.currentUser!!.uid)
                        .set(newUser)
                        .await()
                    mAuth.currentUser!!.sendEmailVerification()
                }
            } catch (e: Exception) {
                shouldDelete = true
                mAuth.currentUser?.delete()     // duplicate call just to be on the safe side

                when (e) {
                    is FirebaseAuthException -> {
                        Log.e(TAG, "Registration error", e)
                        resetDataUnsafe(
                            UNAUTHENTICATED,
                            NotificationMessage(R.string.register_failed_message)
                        )
                        return@withContext
                    }
                    is FirebaseException, is TimeoutCancellationException -> {
                        Log.e(TAG, "Registration network error", e)
                        resetDataUnsafe(
                            UNAUTHENTICATED,
                            NotificationMessage(R.string.network_issue_unspecific)
                        )
                        return@withContext
                    }
                    else -> throw e
                }
            }

            startup()
        }

    private suspend fun startup() = withContext(Dispatchers.Main) {
        val success = loadData()
        if (!success)
            return@withContext

        attachAllSnapshotListeners()
        FCMReceiver.updateFCMToken()
    }

    private suspend fun loadData(): Boolean = withContext(Dispatchers.IO) {
        try {
            loadDataUnsafe()
        } catch (e: FirebaseException) {
            Log.e(TAG, "Possible networking error", e)
            // Although the user is logged in, there is no place to land him
            // other than LoginFragment, for what he has to be signed out.
            safeSignOut(notification = NotificationMessage(R.string.network_issue_unspecific))
            false
        }
    }

    private suspend fun loadDataUnsafe(): Boolean = withContext(Dispatchers.IO) {
        val deferredUser = getUserAsync()
        val deferredWorker = getWorkerAsync()
        val deferredModerator = getModeratorAsync()
        val reloadUser = mAuth.currentUser!!.reload().asDeferred()
        val deferredCategories = getCategoriesFromServerAsync()

        val userTmp = deferredUser.await().toObject<User>()
        if (userTmp == null) {
            // This is either a critical database error
            // or Administrator tried to log in without having User powers.
            Log.e(TAG, "Missing core user data!")
            safeSignOut()
            return@withContext false
        }

        user.safeUpdate(userTmp)
        availableRoles.safeAdd(REGULAR_USER)
        if (userTmp.banned) {
            authenticationState.safeUpdate(BANNED)
            return@withContext false
        }

        // Await after user is cleared to avoid dealing with security exceptions
        val categoriesTmp = deferredCategories.await().toObjects<Category>()
        val deferredProcessCategories = async { safeProcessCategories(categoriesTmp) }

        val workerTmp = deferredWorker.await().toObject<Worker>()
        worker.safeUpdate(workerTmp)
        if (workerTmp != null)
            availableRoles.safeAdd(WORKER)

        val moderatorSnapshot = deferredModerator.await()
        if (moderatorSnapshot.exists())
            availableRoles.safeAdd(MODERATOR)

        currentRole.safeUpdate(getPreferredUserMode()!!)

        deferredProcessCategories.await()

        reloadUser.await()
        authenticationState.safeUpdate(
            if (mAuth.currentUser!!.isEmailVerified) AUTHENTICATED
            else EMAIL_NOT_VERIFIED
        )

        return@withContext true
    }

    private fun getUserAsync(): Deferred<DocumentSnapshot> =
        getDataFromServerAsync("users", mAuth.currentUser!!.uid)

    private fun getWorkerAsync(): Deferred<DocumentSnapshot> =
        getDataFromServerAsync("workers", mAuth.currentUser!!.uid)

    private fun getModeratorAsync(): Deferred<DocumentSnapshot> =
        getDataFromServerAsync("moderators", mAuth.currentUser!!.uid)

    private fun getDataFromServerAsync(
        collectionPath: String,
        documentPath: String
    ): Deferred<DocumentSnapshot> =
        mFirestore.collection(collectionPath)
            .document(documentPath)
            .get(Source.SERVER)
            .asDeferred()

    private fun getCategoriesFromServerAsync(): Deferred<QuerySnapshot> =
        mFirestore.collection("categories")
            .get(Source.SERVER)
            .asDeferred()

    private suspend fun safeProcessCategories(c: List<Category>) = withContext(Dispatchers.Main) {
        val isDefaultLangHR = Locale.getDefault().language == "hr"
        c.forEach { it.titleLocal = if (isDefaultLangHR) it.titleHR else it.titleEN }

        val cSorted = c.toMutableList().apply { sortBy(Category::categoryId) }
        categoriesList = cSorted
        categoriesMap = cSorted.map { it.categoryId to it }.toMap()
    }

    private fun getPreferredUserMode(): Role? {
        val roles = availableRoles.value ?: return null
        return when {
            MODERATOR in roles -> MODERATOR
            WORKER in roles -> WORKER
            REGULAR_USER in roles -> REGULAR_USER
            else -> null
        }
    }

    fun signOut(
        newAuthState: AuthenticationState = UNAUTHENTICATED,
        notification: NotificationMessage? = NotificationMessage(R.string.current_user_signed_out_message)
    ) {
        detachAllSnapshotListeners()
        mAuth.signOut()
        resetDataUnsafe(newAuthState, notification)
    }

    private suspend fun safeSignOut(
        newAuthState: AuthenticationState = UNAUTHENTICATED,
        notification: NotificationMessage? = NotificationMessage(R.string.current_user_signed_out_message)
    ) = withContext(Dispatchers.Main) { signOut(newAuthState, notification) }

    private fun resetDataUnsafe(
        newAuthState: AuthenticationState,
        notification: NotificationMessage? = null
    ) {
        user.value = null
        worker.value = null
        currentRole.value = null
        availableRoles.value?.clear()
        availableRoles.notifyObservers()
        authenticationState.value = newAuthState

        currentlyLoading.value = false
        forceDrawerLock.value = true
        notification?.let { notificationChannel.value = it }

        categoriesList = null
        categoriesMap = null

        filterIconVisible.value = false
        filterRequested.value = null
        selectedCategoryFilter.value = null
    }

    /* START OF VERIFY EMAIL */

    fun emailVerificationStatusRefresh() {
        if (authenticationState.value != EMAIL_NOT_VERIFIED)
            return
        mAuth.currentUser?.reload()?.addOnSuccessListener {
            if (mAuth.currentUser?.isEmailVerified == true) {
                authenticationState.value = AUTHENTICATED
            } else {
                notificationChannel.value = NotificationMessage(R.string.verify_email_notification)
            }
        }
    }

    fun emailVerificationResend() {
        mAuth.currentUser!!.sendEmailVerification()
            .addOnSuccessListener {
                notificationChannel.value =
                    NotificationMessage(R.string.verify_email_resend_success)
            }.addOnFailureListener {
                notificationChannel.value =
                    NotificationMessage(R.string.verify_email_resend_failure)
            }
    }

    /* END OF VERIFY EMAIL */

    /* START OF DEVELOPER CREDITS */

    private val comparator by lazy {
        Comparator<Developer> { d1, d2 ->
            Collator.getInstance(Locale("hr")).compare(d1.lastName, d2.lastName)
        }
    }

    val developers: LiveData<Pair<List<Developer>, List<Developer>>> = liveData {
        currentlyLoading.value = true
        val data = getAllDevelopersSuspend()
        emit(data)
        currentlyLoading.value = false
    }

    private suspend fun getAllDevelopersSuspend(): Pair<List<Developer>, List<Developer>> =
        withContext(Dispatchers.IO) {
            val devs = loadDevelopersFromDatabase()
            val (devsLead, devsNonLead) = devs.partition { it.leader }
            Pair(devsLead.sortedWith(comparator), devsNonLead.sortedWith(comparator))
        }

    private suspend fun loadDevelopersFromDatabase(): List<Developer> =
        mFirestore
            .collection("developerCredits")
            .get()
            .asDeferred()
            .await()
            .toObjects()

    /* END OF DEVELOPER CREDITS */

    /* START OF SETTINGS */

    fun updatePhoneNumber(phone: String) {
        mFirestore.collection("users")
            .document(mAuth.currentUser!!.uid)
            .update("phone", phone)
            .addOnSuccessListener {
                notificationChannel.value = NotificationMessage(R.string.update_phone_success)
            }.addOnFailureListener {
                notificationChannel.value = NotificationMessage(R.string.update_phone_failure)
            }
    }

    fun updatePassword(oldPassword: String, newPassword: String) =
        viewModelScope.launch(Dispatchers.Main) {
            currentlyLoading.value = true
            updatePasswordInternal(oldPassword, newPassword)
            currentlyLoading.value = false
        }

    private suspend fun updatePasswordInternal(
        oldPassword: String,
        newPassword: String
    ) = withContext(Dispatchers.Main) {
        try {
            mAuth.currentUser!!
                .reauthenticate(
                    EmailAuthProvider.getCredential(
                        mAuth.currentUser!!.email!!,
                        oldPassword
                    )
                ).await()
        } catch (e: FirebaseAuthException) {
            notificationChannel.value =
                NotificationMessage(R.string.update_password_failure_incorrect_old)
            return@withContext
        }

        try {
            mAuth.currentUser!!
                .updatePassword(newPassword)
                .await()
        } catch (e: FirebaseAuthException) {
            notificationChannel.value =
                NotificationMessage(R.string.update_password_failure_unknown)
            return@withContext
        }

        notificationChannel.value = NotificationMessage(R.string.update_password_success)
    }

    /* END OF SETTINGS */

    private companion object {
        private val TAG = ActiveUserViewModel::class.java.simpleName
    }

}
