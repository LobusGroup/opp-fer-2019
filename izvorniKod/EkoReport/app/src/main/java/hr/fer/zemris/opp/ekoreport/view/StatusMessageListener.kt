package hr.fer.zemris.opp.ekoreport.view

import android.widget.Toast
import com.google.android.material.snackbar.Snackbar

/** Callback interface for showing various kinds of messages. */
interface StatusMessageListener {

    /**
     * Make and show a standard toast that just contains a text view with the text from a resource.
     *
     * @param resId     The resource id of the string resource to use. Can be formatted text.
     * @param duration How long to display the message.
     *   Either [Toast.LENGTH_SHORT] or [Toast.LENGTH_LONG]
     */
    fun showToast(resId: Int, duration: Int = Toast.LENGTH_LONG)

    /**
     * Make and show a standard toast that just contains a text view.
     *
     * @param text     The text to show.  Can be formatted text.
     * @param duration How long to display the message.
     *   Either [Toast.LENGTH_SHORT] or [Toast.LENGTH_LONG]
     */
    fun showToast(text: CharSequence, duration: Int = Toast.LENGTH_LONG)

    /**
     * Make a Snackbar to display a message.
     *
     * @param resId The resource id of the string resource to use. Can be formatted text.
     * @param duration How long to display the message. Can be [Snackbar.LENGTH_SHORT],
     *   [Snackbar.LENGTH_LONG], [Snackbar.LENGTH_INDEFINITE], or a custom duration in milliseconds.
     * @param showImmediately Whether to immediately show the Snackbar or not
     *
     * @return Created Snackbar
     */
    fun showSnackbar(
        resId: Int,
        duration: Int = Snackbar.LENGTH_LONG,
        showImmediately: Boolean = true
    ): Snackbar

    /**
     * Make a Snackbar to display a message.
     *
     * @param text The text to show. Can be formatted text.
     * @param duration How long to display the message. Can be [Snackbar.LENGTH_SHORT],
     *   [Snackbar.LENGTH_LONG], [Snackbar.LENGTH_INDEFINITE], or a custom duration in milliseconds.
     * @param showImmediately Whether to immediately show the Snackbar or not
     *
     * @return Created Snackbar
     */
    fun showSnackbar(
        text: CharSequence,
        duration: Int = Snackbar.LENGTH_LONG,
        showImmediately: Boolean = true
    ): Snackbar

}
