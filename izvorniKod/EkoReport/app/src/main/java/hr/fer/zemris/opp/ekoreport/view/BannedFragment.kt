package hr.fer.zemris.opp.ekoreport.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import hr.fer.zemris.opp.ekoreport.R
import kotlinx.android.synthetic.main.fragment_banned.*

class BannedFragment : AuthenticationSensitiveFragment<StatusMessageListener>() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_banned, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        tvBannedMessage.text = getString(
            R.string.current_user_banned_message_formatted,
            activeUserViewModel.user.value!!.displayName
        )

        btnBannedSignOut.setOnClickListener { activeUserViewModel.signOut() }

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            requireActivity().finish()
        }
    }

    override fun onBanned() = Unit

}
