package hr.fer.zemris.opp.ekoreport.util

import android.text.format.DateFormat
import com.google.firebase.Timestamp

internal fun Timestamp.getTimeString(): String =
    DateFormat.format("HH:mm:ss", toDate()).toString()

internal fun Timestamp.getDateString(): String =
    DateFormat.format("dd.MM.yyyy.", toDate()).toString()
