package hr.fer.zemris.opp.ekoreport.util

import androidx.annotation.StringRes

data class CustomInputErrors(@StringRes val message: Int, val valid: Boolean)
