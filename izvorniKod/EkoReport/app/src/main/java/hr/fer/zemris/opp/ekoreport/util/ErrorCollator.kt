package hr.fer.zemris.opp.ekoreport.util

import android.content.res.Resources
import com.google.android.material.textfield.TextInputLayout
import java.lang.ref.WeakReference

class ErrorCollator(res: Resources, var errorDetected: Boolean = false) {

    private val mRes = WeakReference(res)

    fun <T> check(s: T, til: TextInputLayout, f: (T) -> CustomInputErrors) {
        f(s).apply {
            if (!this.valid) {
                til.error = mRes.get()!!.getString(this.message)
                errorDetected = true
            }
        }
    }

}
