package hr.fer.zemris.opp.ekoreport.model

data class Developer(
    val firstName: String? = null,
    val lastName: String? = null,
    val linkedin: String? = null,
    val leader: Boolean = false
)
