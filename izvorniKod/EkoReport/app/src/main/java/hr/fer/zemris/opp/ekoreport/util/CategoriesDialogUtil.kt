package hr.fer.zemris.opp.ekoreport.util

import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.lifecycleScope
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import hr.fer.zemris.opp.ekoreport.model.ActiveUserViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

internal fun Fragment.showCategoriesDialog(
    @StringRes extraOptionId: Int,
    @StringRes title: Int,
    selected: MutableLiveData<Int?>,
    isOpen: MutableLiveData<Boolean>,
    activeUserViewModel: ActiveUserViewModel
) {
    val categoriesArray = arrayOf(getString(extraOptionId)) +
            activeUserViewModel.categoriesList!!.map { it.titleLocal }.toTypedArray()

    val checkedItem = with(selected.value) {
        if (this == null) 0
        else categoriesArray.indexOf(
            activeUserViewModel.categoriesMap!!.getValue(this).titleLocal
        )
    }

    // Dialog must take care of resetting "isOpen" when being closed
    MaterialAlertDialogBuilder(requireContext())
        .setTitle(title)
        .setSingleChoiceItems(categoriesArray, checkedItem) { dialog, which ->
            selected.value =
                if (which == 0) null
                else activeUserViewModel.categoriesList!![which - 1].categoryId

            viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Main.immediate) {
                isOpen.value = false
                // Delay automatic closing by 500ms to have time to show
                // the radio button animation and finish loading filtered data.
                delay(500L)
                dialog.dismiss()
            }
        }.setOnCancelListener { isOpen.value = false }
        .setOnDismissListener { isOpen.value = false }
        .create()
        .show()
}
