package hr.fer.zemris.opp.ekoreport.adapter

import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.firebase.ui.firestore.FirestoreRecyclerAdapter
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import hr.fer.zemris.opp.ekoreport.R
import hr.fer.zemris.opp.ekoreport.model.PhoneNumber
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.phone_number.view.*
import java.util.*

class FirestorePhoneNumberAdapter(options: FirestoreRecyclerOptions<PhoneNumber>) :
    FirestoreRecyclerAdapter<PhoneNumber, FirestorePhoneNumberAdapter.ViewHolder>(options) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.phone_number, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int, item: PhoneNumber) {
        holder.apply {
            tvName.text = if (Locale.getDefault().language == "hr") item.nameHR else item.nameEN
            tvPhone.text = item.phone

            btnPhoneIcon.setOnClickListener {
                val intent = Intent(Intent.ACTION_DIAL)
                intent.data = Uri.parse("tel:${item.phone}")
                it.context.startActivity(intent)
            }
        }
    }

    inner class ViewHolder(override val containerView: View) :
        RecyclerView.ViewHolder(containerView),
        LayoutContainer {

        val tvName: TextView = containerView.tvName
        val tvPhone: TextView = containerView.tvPhone
        val btnPhoneIcon: Button = containerView.btnPhoneIcon
    }

}
