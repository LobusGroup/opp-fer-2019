package hr.fer.zemris.opp.ekoreport.model

import android.content.Context
import android.location.Geocoder
import android.net.Uri
import android.util.Log
import android.view.View
import android.widget.ImageView
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.EventListener
import com.google.firebase.firestore.GeoPoint
import com.google.firebase.firestore.ListenerRegistration
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.ktx.storage
import com.stfalcon.imageviewer.StfalconImageViewer
import hr.fer.zemris.opp.ekoreport.util.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.text.SimpleDateFormat
import java.util.*

class ActiveReportViewModel : ViewModel() {

    private val mFirestore by lazy { Firebase.firestore }
    private val mStorage by lazy { Firebase.storage }

    /** Currently active report. */
    val report by lazy { MutableLiveData<Report?>() }

    val leaveReportImmediately by lazy { MutableLiveData<Boolean>(false) }

    val currentlySubmittingReport by lazy { MutableLiveData<Boolean>(false) }
    val receivedLocation by lazy { MutableLiveData<Boolean>(false) }
    val address by lazy { MutableLiveData<String?>(null) }

    val categoryChooserOpen by lazy { MutableLiveData(false) }
    val selectedCategoryId by lazy { MutableLiveData<Int?>(null) }

    private var sharedImageUri: Uri? = null
    private var imageUri: Uri? = null

    private var location: GeoPoint? = null

    private var reportsListener: ListenerRegistration? = null

    private val reportEventListener = EventListener<DocumentSnapshot> { snapshot, e ->
        if (e != null) {
            Log.e(TAG, "reportEventListener:error", e)
            return@EventListener
        }

        if (snapshot?.exists() == true) {
            report.update(snapshot.toObject<Report>()!!)
            selectedCategoryId.update(report.value!!.category)
        } else {
            // Report is deleted
            leaveReportImmediately.update(true)
            setActiveReport(null)
            Log.e(TAG, "Missing core report data!")
        }
    }

    fun shareImage(uri: Uri) {
        setActiveReport(null)
        sharedImageUri = uri
    }

    fun hasShareImage(): Boolean = sharedImageUri != null

    fun finishShare() {
        sharedImageUri = null
    }

    fun getSharedImageUri(): Uri = sharedImageUri!!

    fun setActiveReport(newReport: Report?) {
        sharedImageUri = null
        receivedLocation.update(false)
        currentlySubmittingReport.update(false)

        imageUri = null
        location = null
        address.update(null)

        report.update(newReport)
        selectedCategoryId.update(newReport?.category)
        categoryChooserOpen.update(false)

        detachListener(::reportsListener)
        newReport ?: return

        reportsListener = attachListener(
            "reports",
            newReport.documentId,
            reportEventListener
        )
    }

    fun updateActiveReport(map: Map<String?, Any?>) {
        report.value ?: return
        mFirestore.collection("reports")
            .document(report.value!!.documentId)
            .update(map)
    }

    fun deleteCurrentReport() {
        report.value ?: return
        mFirestore.collection("reports")
            .document(report.value!!.documentId)
            .delete()
    }

    private fun createNewReport(newReport: Report) {
        mFirestore.collection("reports")
            .add(newReport)
    }

    fun downloadImageInto(iv: ImageView) = downloadImageInto(iv, report.value!!.imageReport)

    fun downloadImageInto(iv: ImageView, reportName: String, attachListener: Boolean = false) {
        report.value ?: return

        // Download and display report image with placeholder loader
        val photoReference = mStorage.getReference(reportName)

        loadImageRemote(photoReference, iv)
        iv.visibility = View.VISIBLE

        if (attachListener) {
            iv.setOnClickListener {
                StfalconImageViewer.Builder<StorageReference>(
                    iv.context,
                    listOf(photoReference)
                ) { view, image ->
                    loadImageRemote(image, view)
                }.withTransitionFrom(iv)
                    .show()
            }
        }
    }

    fun loadImage(iv: ImageView, uri: Uri? = imageUri) {
        imageUri = uri
        loadImageLocal(uri, iv)
    }

    fun hasPhoto() = imageUri != null

    fun setLocation(loc: GeoPoint, context: Context) {
        location = loc
        address.update(null)

        viewModelScope.launch(Dispatchers.Main) { getAddress(loc, context) }
    }

    private suspend fun getAddress(loc: GeoPoint, context: Context) = withContext(Dispatchers.IO) {

        fun attemptOnce(): String? =
            try {
                Geocoder(context, Locale("hr")).getFromLocation(
                    loc.latitude,
                    loc.longitude,
                    1
                )[0].getAddressLine(0)
            } catch (e: Exception) {
                Log.e(TAG, e.toString())
                null
            }

        var addr: String? = null
        for (i in 0 until geocoderMaxAttempts) {
            addr = attemptOnce()
            if (addr != null)
                break
        }

        address.safeUpdate(addr ?: unknownAddressFallback)
        receivedLocation.safeUpdate(true)
    }

    fun submitChanges(uid: String, title: String, description: String, category: Int) {

        currentlySubmittingReport.update(true)

        if (report.value == null) {

            if (location == null) {
                currentlySubmittingReport.update(false)
            } else {
                if (address.value == null) address.update(unknownAddressFallback)

                val ts: String = SimpleDateFormat("yyyyMMdd_HHmmss", Locale("en"))
                    .format(Date())
                val reportImageName = "REPORT_$ts.jpg"
                val imageReport = "reportImages/${uid}/${reportImageName}"
                val reportImagesRef = mStorage.reference.child(imageReport)

                // Upload photo to storage
                reportImagesRef.putFile(imageUri!!).addOnSuccessListener {
                    // Create new report
                    val newReport = Report(
                        uidUser = uid,
                        location = location!!,
                        address = address.value!!,
                        title = title,
                        description = if (description.isNotEmpty()) description else null,
                        category = category,
                        imageReport = imageReport
                    )
                    createNewReport(newReport)
                    currentlySubmittingReport.update(false)
                }.addOnFailureListener {
                    Log.d(TAG, "Report image upload failed!", it)
                    currentlySubmittingReport.update(false)
                }
            }

        } else {
            updateActiveReport(
                mapOf(
                    "title" to title,
                    "description" to if (description.isNotEmpty()) description else null,
                    "category" to category
                )
            )
            currentlySubmittingReport.update(false)
        }
    }

    private companion object {
        private val TAG = ActiveReportViewModel::class.java.simpleName

        private const val unknownAddressFallback = "Nepoznato"
        private const val geocoderMaxAttempts = 3
    }

}
