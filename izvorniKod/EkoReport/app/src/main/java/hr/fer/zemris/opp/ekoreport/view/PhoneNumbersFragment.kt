package hr.fer.zemris.opp.ekoreport.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.ktx.Firebase
import hr.fer.zemris.opp.ekoreport.R
import hr.fer.zemris.opp.ekoreport.adapter.FirestorePhoneNumberAdapter
import hr.fer.zemris.opp.ekoreport.model.PhoneNumber
import kotlinx.android.synthetic.main.fragment_phone_numbers.*

class PhoneNumbersFragment : Fragment() {

    private val mFirestore by lazy { Firebase.firestore }
    private lateinit var adapter: FirestorePhoneNumberAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_phone_numbers, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val query = mFirestore
            .collection("phoneNumbers")
            .orderBy("emergency", Query.Direction.DESCENDING)
            .orderBy("phone", Query.Direction.ASCENDING)

        val options = FirestoreRecyclerOptions.Builder<PhoneNumber>()
            .setQuery(query) { it.toObject<PhoneNumber>()!! }
            .setLifecycleOwner(viewLifecycleOwner)
            .build()

        adapter = FirestorePhoneNumberAdapter(options)
        rvAllNumbers.layoutManager = LinearLayoutManager(requireContext())
        rvAllNumbers.adapter = adapter
        adapter.startListening()
    }

    override fun onDestroyView() {
        super.onDestroyView()

        // Should be done automatically, but we do it explicitly to have cleaner code
        adapter.stopListening()
    }

}
