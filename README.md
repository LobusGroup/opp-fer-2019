# Project repository for course "Software Design" @ FER

**Documentation:** ![](https://gitlab.com/LobusGroup/opp-fer-2019/badges/devdoc/pipeline.svg "Devdoc pipeline status")  
**Android App:** ![](https://gitlab.com/LobusGroup/opp-fer-2019/badges/develop/pipeline.svg "Develop pipeline status")

### GitLab Pages: <https://lobusgroup.gitlab.io/opp-fer-2019/>
Documentation v1.0: [link](https://gitlab.com/LobusGroup/opp-fer-2019/blob/431d5426ff81a266ede4277bfddbd9817b437c32/dokumentacija/OPP_2019_LobusGroup_v1.pdf)  
Documentation v2.0: [link](https://gitlab.com/LobusGroup/opp-fer-2019/blob/fd9bf3e4aa43f09b1c2293d89745707cb9b18dff/OPP_2019_LobusGroup_v2_0.pdf)  
Presentation: [link](https://gitlab.com/LobusGroup/opp-fer-2019/blob/master/dokumentacija/OPP_2019_LobusGroup_Prezentacija.pptx)  

Google Play: [link](https://play.google.com/store/apps/details?id=hr.fer.zemris.opp.ekoreport)  

\
This app is a university project at Faculty of Electrical Engineering and Computing in Zagreb in 2019/2020.

Through a simple and modern interface, users can report environmental issues. 
Sea and beach pollution, collapsed trees and overgrown greenery, ice and snow 
on roads and driveways, destroyed traffic signage, scattered trash and bulky waste, 
dead animals on and off the road, stray dogs and alike - all the problems we come 
across every day and think someone else will report them can now be solved quickly 
and easily using this application. There is no need to look up the numbers and 
call public utilities, because all the user has to do is take a picture of the problem, 
select a category and have location tracking activated on their mobile device. 
The app takes care of everything else. 
Depending on the category, the problem is forwarded to the responsible service 
whose employees are also logged into the application and see reports in their jurisdiction.

\
<img src="grafike/EkoReportLogo.png" width="300" />
